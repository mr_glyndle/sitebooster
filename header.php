<!doctype html>
<?php
$GLOBALS['footer-js'] = "";
$GLOBALS['footer-css'] = ".override .content.main,.override .lazyload,.override .lazyloaded,.override .video.controls {opacity: 1;}";
$GLOBALS['footer-schema'] = '';
$site_width = get_field('site_width', 'option');
$header_backround_colour = get_field('header_backround_colour', 'option');
if (get_field('header_text_colour', 'option')) {
	$nav_text_colour = get_field('header_text_colour', 'option');
}
$force_ssl = get_field('force_ssl', 'option');
if ($force_ssl) {
	$force_ssl = $force_ssl[0];
}
unset($head_set);
$head_set = get_field('head_set');
$page_width = get_field('head_set_override_site_width');
$head_code = get_field('head_set_head_code');

if(isset($head_set['override_site_width']) && $head_set['override_site_width'] !== '' && $head_set['override_site_width'] !== 'default') {
	$site_width = $head_set['override_site_width'];
};
$background_image_for_site = '';
$background_colour_for_site = '';
if ($site_width == 'contained') {
	$background_image_for_site = get_field('background_image_for_site', 'option');
	$background_colour_for_site = get_field('background_colour_for_site', 'option');
};
?>
<html id="brandchap" <?php language_attributes(); ?> class="override <?php if ($site_width) { echo $site_width . ' '; }; ?>">
<?php $GLOBALS['footer-css'] .= 'html {'; if ($background_image_for_site && $site_width == 'contained') { $GLOBALS['footer-css'] .= 'background:url(' . $background_image_for_site['url'] . ') no-repeat fixed; background-position: top center; background-size: cover;'; } if ($background_colour_for_site) { $GLOBALS['footer-css'] .= 'background-color : ' .  $background_colour_for_site . ' !important;'; }; $GLOBALS['footer-css'] .= '}';?>

<head>
	<?php
	// Inject any custom header code for this page?
	if($head_code && $head_code !== '') {
		echo $head_code;
	}
	?>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="HandheldFriendly" content="true"/>
	<?php
			// Load lazy load script
			echo '<script>';	include(locate_template('/js/lazy.js')); echo '</script>';

		$business_desc = get_field('business_desc', 'option');
		if (isset($business_desc)) { echo '<meta name="subject" content="' . $business_desc . '">'; };

	echo '<style>';
		// Conditionally load main style inline to control menu layout type and speed up page load
		$fixed_nav = get_field('fixed_nav', 'option');

		if (is_page_template('no_chrome.php')) {
			$fixed_nav = 'mini';
		}

		if($fixed_nav == "mini") {
			// Mini menu Style
			include(locate_template('mobile.css'));
		} else if(($fixed_nav == "standard" || $fixed_nav == "fixed_nav")) {
			// Standard Responsive Style
			include(locate_template('responsive.css' ));
		}
	echo '</style>';

	if (is_front_page()) {

		$GLOBALS['footer-schema'] .= '"@context" : "http://schema.org",
		"@type" : "' . get_field('schema_type', 'options') . '",
		"name" : "' . get_bloginfo('name') . '",
		"url" : "' . get_home_url() . '",
		"telephone" : "+44' . get_field('company_phone', 'options') . '",
		"address" : {
			"@type" : "PostalAddress",
			"streetAddress" : "' . get_field('address_street', 'option') . '",
			"postalCode" : "' . get_field('address_postal', 'option') . '",
			"addressLocality" : "' . get_field('address_locality', 'option') . '",
			"addressRegion" : "' . get_field('address_region', 'option') . '",
			"addressCountry" : "' . get_field('address_country', 'option'). '"
		},
		"priceRange" : "£' . get_field('pricerange_min', 'option') . ' - £' . get_field('pricerange_max', 'option') . '"';

		/// HOTEL OPTIONS
		if (get_field('schema_type', 'options') == 'Hotel') {
			$GLOBALS['footer-schema'] .= ',"starRating" : {
				"@type" : "Rating",
				"ratingValue" : "' . get_field('starrating', 'option') . '"
			}';
		}
		if(get_the_post_thumbnail_url()) {
			$GLOBALS['footer-schema'] .= ',"image": {
				"@type": "imageObject",
				"url": "' . get_the_post_thumbnail_url(get_the_ID(),'golden_medium') . '",
				"height": "632px",
				"width": "898px"
			}';
		}
		$GLOBALS['footer-schema'] .= ',"sameAs":[';

		$social_schema = get_field('social_media', 'option');
		if ($social_schema) {
			$ssn = count($social_schema);
			$x = 1;
			foreach ($social_schema as $url) {
				$GLOBALS['footer-schema'] .= '"' . $url['url'] . '"';
				if ($x < $ssn) {
					$GLOBALS['footer-schema'] .= ',';
				}
				++$x;
			}
		}
		$GLOBALS['footer-schema'] .= ']';

		/// OPENING HOURS
		if (have_rows('opening_hours', 'option')) {

			$opening_hour_total =  count(get_field('opening_hours', 'option'));
			$opening_hour_tally = 1;

			$GLOBALS['footer-schema'] .= ',"openingHoursSpecification": [';

	        while (have_rows('opening_hours', 'option')) : the_row();

				if (get_sub_field('closed') !== 1) {

	                $days = get_sub_field('days');

					$Day_Count  = count($days);
					$Day_Tally = 1;

					foreach ($days as $day) {

						if ($day == 'Mo') {
							$day = 'Monday';
						} else if ($day == 'Tu') {
							$day = 'Tuesday';
						} else if ($day == 'We') {
							$day = 'Wednesday';
						} else if ($day == 'Th') {
							$day = 'Thursday';
						} else if ($day == 'Fr') {
							$day = 'Friday';
						} else if ($day == 'Sa') {
							$day = 'Saturday';
						} else if ($day == 'Su') {
							$day = 'Sunday';
						}
						$GLOBALS['footer-schema'] .= '{ "@type": "OpeningHoursSpecification",';
						$GLOBALS['footer-schema'] .= '"closes":  "' . get_sub_field('to') . '",';
						$GLOBALS['footer-schema'] .= '"dayOfWeek": "http://schema.org/' . $day . '",';
					    $GLOBALS['footer-schema'] .= '"opens": "' . get_sub_field('from') . '"}';
						if(($opening_hour_tally == $opening_hour_total) && ($Day_Tally == $Day_Count)) {

						} else {
							$GLOBALS['footer-schema'] .= ',';
						}
						$Day_Tally++;
					}
				}


				$opening_hour_tally++;

	        endwhile;

			$GLOBALS['footer-schema'] .= ']';

	        /// VACATIONS / HOLIDAYS
	        if (have_rows('special_days', 'option')) {

	            while (have_rows('special_days', 'option')) : the_row();

	                $closed    = get_sub_field('closed');
	                $date_from = get_sub_field('date_from');
	                $date_to   = get_sub_field('date_to');
	                $time_from = $closed ? '00:00' : get_sub_field('time_from');
	                $time_to   = $closed ? '00:00' : get_sub_field('time_to');

	                $special_days = array(
	                    '@type'        => 'OpeningHoursSpecification',
	                    'validFrom'    => $date_from,
	                    'validThrough' => $date_to,
	                    'opens'        => $time_from,
	                    'closes'       => $time_to
	                );

	                array_push($schema['openingHoursSpecification'], $special_days);

	            endwhile;
	        }
	    }


	} else if ( (!is_singular('accom') && !is_page_template( 'menu_page.php' )) && (is_page() || is_single())) {

		$GLOBALS['footer-schema'] .= '"@context": "http://schema.org"';

		$content_type = get_field( 'head_set_content_type' );
		if (is_array($content_type)) {
			$content_type = $content_type[0];
		}

		if ( $content_type !== 'Article' ) {
			$GLOBALS['footer-schema'] .= ',"@type": "' . $content_type . '"';
		} else {
			$GLOBALS['footer-schema'] .= ',"@type": "Article"';
		}
		if ( $content_type == 'Product' ) {
			$name_type = 'name';
			$from_name = 'brand';
		} else if ( $content_type == 'Service' ) {
			$name_type = 'serviceType';
			$from_name = 'provider';
		} else {
			$name_type = 'headline';
			$from_name = 'publisher';
		}


		if(get_the_title()) { $GLOBALS['footer-schema'] .= ',"' . $name_type . '": "' . get_the_title() . '"'; }
		if(get_permalink()) {
			$GLOBALS['footer-schema'] .= ', "@id": "' . get_permalink() . '"';
		}
		if(get_the_post_thumbnail_url()) {
			$GLOBALS['footer-schema'] .= ', "image": {
				"@type": "imageObject",
				"url": "' . get_the_post_thumbnail_url(get_the_ID(),'golden_medium') . '",
				"height": "632px",
				"width": "898px"
			}';
		}
		if( get_the_author() && $content_type == 'Article') {
			$GLOBALS['footer-schema'] .= ',"author": "' . get_the_author() . '"';
		} else if( !get_the_author() && $content_type == 'Article') {
			$GLOBALS['footer-schema'] .= ',"author": "' . get_bloginfo('name') . '"';
		}
		$GLOBALS['footer-schema'] .= ',"' . $from_name . '": {
			"@type": "Organization",
			"name": "' . get_bloginfo('name') . '",
			"@id":"' . get_home_url() . '#organization",
			"logo": {
				"@type": "imageObject",
				"url": "' . get_field('company_logo', 'option') . '"
			},
			"sameAs":[';

		$social_schema = get_field('social_media', 'option');
		if ($social_schema) {
			$ssn = count($social_schema);
			$x = 1;
	        foreach ($social_schema as $url) {
				$GLOBALS['footer-schema'] .= '"' . $url['url'] . '"';
				if ($x < $ssn) {
					$GLOBALS['footer-schema'] .= ',';
				}
				++$x;
	        }
	    }
		$GLOBALS['footer-schema'] .= ']
		}';

		$categories = get_the_category();
		if ( ! empty( $categories ) ) {
			$GLOBALS['footer-schema'] .= ', "genre": "' . esc_html( $categories[0]->name ) . '"';
		}
		if(get_permalink()) {
			$GLOBALS['footer-schema'] .= ', "url": "' . get_permalink() . '"';
		}
		if( $content_type == 'Article') {
			if(get_the_date('Y-m-d')) {
				$GLOBALS['footer-schema'] .= ', "datePublished": "' . get_the_date('Y-m-d') . '"';
			}
			if(get_the_date('Y-m-d')) {
				$GLOBALS['footer-schema'] .= ', "dateCreated": "' . get_the_date('Y-m-d') . '"';
			}
			if(get_the_modified_date('Y-m-d')) {
				$GLOBALS['footer-schema'] .= ', "dateModified": "' . get_the_modified_date('Y-m-d') . '"';
			}
		}
		if(get_the_excerpt()) {
			$GLOBALS['footer-schema'] .= ', "description": "' . get_the_excerpt() . '"';
		}
		$GLOBALS['footer-schema'] .= ',"mainEntityOfPage": {
	         "@type": "WebPage",
	         "@id": "' . get_permalink() . '"
		}';
	}

	if( !current_user_can('editor') && !current_user_can('administrator') ) {
		$tracking_code_head = get_field('tracking_code', 'option');
		if ($tracking_code_head) {
			echo $tracking_code_head;
		}
	}

	// Set app bar colour to match theme or custom set header colours
	// If header background colour is set...
	if (isset($head_set['bg_col']) && $head_set['bg_col'] !== ''){

			// Android
			echo '<meta name="theme-color" content="' . $head_set['bg_col'] . '">';
			// Windows Phone
			echo '<meta name="msapplication-navbutton-color" content="' . $head_set['bg_col'] . '">';

	// If header overlay colour is set...
	} else if (isset($head_set['over_color']) && $head_set['over_color'] !== ''){

			// Android
			echo '<meta name="theme-color" content="' . $head_set['over_color'] . '">';
			// Windows Phone
			echo '<meta name="msapplication-navbutton-color" content="' . $head_set['over_color'] . '">';

	// If sitewide header colour is set...
	} else if ($header_backround_colour) {

		// Android
		echo '<meta name="theme-color" content="' . $header_backround_colour . '">';
		// Windows Phone
		echo '<meta name="msapplication-navbutton-color" content="' . $header_backround_colour . '">';

	}; ?>

	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<?php	wp_head();
	if ($header_backround_colour) { $GLOBALS['footer-css'] .= 'header.top ul.sub-menu, .main-nav ul.sub-menu, nav.main-nav, #breadcrumbs {background:' . $header_backround_colour . ';}'; };

	$load_font = get_field('load_font', 'option');
	if ($load_font) {
		if (strpos($load_font, 'googleapis') !== false) {
			$fonts = explode('family=', $load_font);
			$fonts = explode('"', $fonts[1]);
			$fonts = explode('|', $fonts[0]);?>
			<script>WebFontConfig = {
					google: { families: [ <?php foreach ($fonts as &$font) { echo "'" . $font . "',"; } ?> ]
				   }
				};
			   (function(d) {
			      var wf = d.createElement('script'), s = d.scripts[0];
			      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
			      wf.async = true;
			      s.parentNode.insertBefore(wf, s);
			   })(document);
			</script><?php } else { echo $load_font; } }; ?>

  <!--[if lt IE 10]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
	<style>img {max-width: none;}</style>
  <![endif]-->

</head>

<body <?php body_class(isset($class));?>>
	<?php if( !current_user_can('editor') && !current_user_can('administrator') ) {
		$tracking_code_body = get_field('tracking_code_body', 'option');
			if ($tracking_code_body) {
				echo $tracking_code_body;
			}
		}; ?>
	<?php $link_to_svg = get_template_directory_uri () . '/img/symbol-defs.svg';?>
	<div id="wrapper" class="<?php if($site_width) {echo $site_width;};?>">

		<?php if (is_page_template('no_chrome.php')) {
			echo '<header class="nochrome ' . $nav_text_colour . '">';
			include(locate_template('partials/nav/logo.php'));
			include(locate_template('partials/nav/nav.php'));
			echo '</header>';
		} else if (!is_page_template('no_chrome.php')) { ?>

		<header <?php if($header_backround_colour && ( $head_set['head_bg'] !== '' || $head_set['head_bg'] !== "custom")) { echo 'style=" background-color: ' . $header_backround_colour . ';"'; } ?> class="top text parallax <?php if($nav_text_colour) { echo $nav_text_colour; }?><?php if ($head_set['head_bg'] == "custom") { echo " custom"; }?>" data-nav-status="toggle" >
			<div class="hat">
				<?php

				if ( is_search() ) {

					include(locate_template('partials/nav/logo.php'));?>

					<div class="s_over">
						<div class="content txt_blk avs_default">
							<?php echo '<h3 style="text-align: center;">You searched for: "<i>' . get_search_query() . '</i>"</h3>'; ?>
						</div>
					</div>

				<?php } else {

					if (is_category() || is_tax() || is_tag() || is_archive() ) {

						include(locate_template('partials/nav/logo.php'));

						if (function_exists ( "is_woocommerce" ) && is_realy_woocommerce_page() ) {

						} else {

							include(locate_template('partials/category/jumbo_header.php'));
							$is_a_cat = true;

						}

					} else if ($head_set['head_bg'] == "custom") {

						include(locate_template('partials/section_background.php'));
						include(locate_template('partials/overlay.php'));

					} else if( ( class_exists( 'Tribe__Events__Main' ) || class_exists( 'Tribe__Events__Pro__Main' ) ) && ( tribe_is_event() && is_single() ) ) {

						include(locate_template('partials/event_header.php'));

					} else {

						unset($rgba_colour);

					}

					if (!isset($is_a_cat)) { ?>

						<?php include(locate_template('partials/nav/logo.php')); ?>

						<?php if (isset($rgba_colour) && !$rgba_colour == '') { echo '<div style="background-color:' . $rgba_colour . '" class="s_over">'; }?>

							<div class="content txt_blk <?php if ( isset($head_set) && $head_set['add_spacing'] ) { echo 'avs_' . $head_set['add_spacing']; }
							elseif ( ( (class_exists( 'Tribe__Events__Main' ) && tribe_is_event() ) && is_single() ) ) { echo 'avs_10'; } else { echo 'avs_default';}; ?>">

								<?php if ( isset($head_set) ) {
									if ($head_set['header_content'] == 'title') {
										the_title( '<h1 style="text-align: center;">', '</h1>' );
									} else if ( $head_set['header_content'] == 'custom') {
										echo $head_set['head_cont'];
									}
								} else if( ( class_exists( 'Tribe__Events__Main' ) || class_exists( 'Tribe__Events__Pro__Main' ) ) && ( tribe_is_event() && is_single() ) ) {
									echo '<h1 style="text-align: center;">' . tribe_get_events_title() . '</h1></div>';
								} else {
								}?>

							<?php if (isset($rgba_colour) && !$rgba_colour == '') { '</div>'; } ?>
						</div>

						<!-- close Section background -->
						<?php if ($head_set['head_bg'] == "custom") { echo "</div>"; }

					}
					unset($head_set);
				}?>
			</div>

			<!-- NAV -->
			<?php $fixed_nav = get_field('fixed_nav', 'option');
			if ( $fixed_nav == 'mini' || $fixed_nav == 'standard' ) {
				include(locate_template('partials/nav/nav.php'));
			} else if ( $fixed_nav == 'fixed_nav') {
				include(locate_template('partials/nav/fixed.php'));
			}?>

		</header>
		<div class="content main">
			<?php $no_bread = get_field('head_set_no_bread');
			if ( !is_front_page() && !is_category() && !is_tax('accommodation_type') && empty($no_bread) ) {
				if ( function_exists('yoast_breadcrumb') ) {
					if ($nav_text_colour) {
						$breadcrumb_color = '<p id="breadcrumbs" class="' . $nav_text_colour . '">';
						yoast_breadcrumb($breadcrumb_color,'</p>');
					} else {
						yoast_breadcrumb('<p id="breadcrumbs" class="">','</p>');
					}
				}
			}
		} else { // close no chrome logic ?><div class="content main"><?php }; ?>
