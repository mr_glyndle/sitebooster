<?php
/**
 * Default Events Template
 * This file is the basic wrapper template for all the views if 'Default Events Template'
 * is selected in Events -> Settings -> Template -> Events Template.
 *
 * Override this template in your own theme by creating a file at [your-theme]/tribe-events/default-template.php
 *
 * @package TribeEventsCalendar
 *
 */
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
get_header();
?>
<div id="tribe-events-pg-template" class="tribe-events-pg-template">
	<?php
	if( !is_single() ) {
		$args = array(
			'taxonomy' => 'tribe_events_cat',
			'title_li' => '',
			'hierarchical' => 1,
		);
		echo '<nav class="filters"><ul class="inline-block vmiddle filter menu"><li class="menu-item-has-children dropdown"><a href="#category">Change Event category</a><ul class="sub-menu">';
		wp_list_categories( $args );
		echo '</ul></li></ul></nav>';
	}
	tribe_events_before_html(); ?>
	<?php tribe_get_view(); ?>
	<?php tribe_events_after_html(); ?>
</div> <!-- #tribe-events-pg-template -->
<?php
get_footer();

$filename = get_stylesheet_directory_uri() . '/css/events/events.css';
$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
$filenonroot = str_replace($root,"",$filename);
if (file_exists($filenonroot)) {
    $filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
}
echo '<link rel="stylesheet" href="' . $filename . '">';
