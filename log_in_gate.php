<?php
/*
Template Name: Login Gate
Description: Use this to make the content hidden from none logged in users
*/
get_header();

if ( is_user_logged_in() ) {

		if (have_posts()) { ?>

		<article>

			<div class="container main">
			<?php while (have_posts()) : the_post(); ?>

				<?php

		    // check if the flexible content field has rows of data
		    if( have_rows('cont') ) {

				$item_count = 1;
				$GLOBALS['item_count'] = "1";

				// loop through the rows of data
				while ( have_rows('cont') ) : the_row();
					include(locate_template('partials/slice_loop.php'));
				endwhile;

			} else {
				echo '<div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix"><div class="text_content">';
				echo the_content();
				echo '</div></div></div>';
			}

			 if (!is_page() && is_singular()) {
				 ?><div class="txt_blk page_nav clearfix"><div class="alignleft"><?php previous_post_link('%link', '&laquo; Prev', TRUE); ?></div><div class="alignright"><?php next_post_link('%link', 'Next &raquo;', TRUE); ?></div></div><?php the_tags( '<div class="txt_blk page_nav tags clearfix">Tags: ', ' | ', '</div>' ); if (comments_open() || get_comments_number()) {
					 ?><div class="txt_blk comments"><?php comments_template(); ?></div><?php
				 }
			 }

		 	endwhile;

	 	} else {

		 ?><div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix">
					<div class="text_content">
						<div class="alert alert-info">
						  <h1>Sorry, we can't find the page you're looking for</h1>
							<p>Please use the navigation or, seach the site with the options above.</p>
						</div>
					</div>
				</div><?php

		}

		?></div>

	</article><?php


	} else {

	    echo '<div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix">
			<div class="text_content" style="text-align:center;"><h4>To view this content you will need to be logged in:</h4><br>';

		$args = array(
			'echo'           => true,
			'remember'       => true,
			'redirect'       => ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'],
			'form_id'        => 'loginform',
			'id_username'    => 'user_login',
			'id_password'    => 'user_pass',
			'id_remember'    => 'rememberme',
			'id_submit'      => 'wp-submit',
			'label_username' => 'Username',
			'label_password' => 'Password',
			'label_remember' => 'Remember Me',
			'label_log_in'   => 'Log In',
			'value_username' => '',
			'value_remember' => false
		);

		wp_login_form( $args );
		echo '</div></div></article></div>';

	}

	get_footer(); ?>
