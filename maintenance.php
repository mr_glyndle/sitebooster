<?php header('HTTP/1.1 503 Service Temporarily Unavailable'); header('Status: 503 Service Temporarily Unavailable'); header('Retry-After: 600'); ?>
<!DOCTYPE html>
<?php
unset($head_set);
$head_set = get_field('head_set');
$background_image_for_site = get_field('background_image_for_site', 'option');
$background_colour_for_site = get_field('background_colour_for_site', 'option');
?>
<html>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<?php $business_desc = get_field('business_desc', 'option');
	if (isset($business_desc)) { echo '<meta name="subject" content="' . $business_desc . '">'; }; ?>

	<?php
	if ( !is_user_logged_in() ) {
		$tracking_code_head = get_field('tracking_code', 'option');
		if ($tracking_code_head) {
			echo $tracking_code_head;
		};
	};

	$load_font = get_field('load_font', 'option');
	if ($load_font) {
		if (strpos($load_font, 'googleapis') !== false) {
			$fonts = explode('family=', $load_font);
			$fonts = explode('"', $fonts[1]);
			$fonts = explode('|', $fonts[0]);?>
			<script>WebFontConfig = {
					google: { families: [ <?php foreach ($fonts as &$font) { echo "'" . $font . "',"; } ?> ]
				   }
				};
			   (function(d) {
			      var wf = d.createElement('script'), s = d.scripts[0];
			      wf.src = 'https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js';
			      wf.async = true;
			      s.parentNode.insertBefore(wf, s);
			   })(document);
			</script>
		<?php } else { echo $load_font; } }; ?>


	<style>
	<?php include(locate_template('responsive.css' )); ?>
	.container {
		<?php
		if (isset($background_image_for_site)) {
			echo 'background-image: url("' . $background_image_for_site['sizes']['square_large'] . '"); background-position: center center; background-size: cover;filter: blur(1em); -webkit-filter: blur(1em); position: absolute; top: -2em; right: -2em; bottom: -2em; left: -2em; position: fixed; z-index: 1;';
		}
		// if (isset($background_colour_for_site)) {
		// 	echo 'background-color: ' . $background_colour_for_site . ';';
		// }
		?>
	}
	.content {
		position: absolute;
		z-index: 2;
		top: 50%;
		left: 50%;
		transform: translateX(-50%) translateY(-60%);
		max-width: 40em;
		padding: 2em;
	}
	</style>

</head>

<body>
	<div class="container">
	</div>
	<div class="content">
		<?php
			// include(locate_template('partials/nav/logo.php'));
			echo get_field('down_cont', 'option');
		?>
	</div>
	<style>
	<?php include(locate_template('style.css')); ?>
	</style>
</body>
</html>
