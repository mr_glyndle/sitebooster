<?php
include(locate_template('partials/section_background.php'));
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));
$full_width = get_sub_field('full_width');
$full_width = $full_width[0];
$intro_text = get_sub_field('intro_text');

$grid_item_count = 1;
$number_of_tables = 0;
// check if the repeater field has rows of data
if( have_rows('comp-table') ):
	// loop through the rows of data
	while ( have_rows('comp-table') ) : the_row();
		++$number_of_tables;
	endwhile;
endif;

$container_class = 'grid responsive no_overlay';
if ($number_of_tables % 2 == 0) {
	$odd_or_even = 'even';
} else {
	$odd_or_even = 'odd';
};
if ($full_width == 'yes') {
		$container_class = $container_class . ' full';
};
$container_class = $container_class . ' ' . $odd_or_even;
$container_class = $container_class . ' ti_' . $number_of_tables;

if($intro_text) {
 echo '<div class="intro txt_blk">' . $intro_text . '</div>';
};
?>

<div class="<?php echo $container_class ?>">

	<?php

	// check if the repeater field has rows of data
	if( have_rows('comp-table') ) {
	 	// loop through the rows of data
	  while ( have_rows('comp-table') ) : the_row();?>
		<dl class="item comp-table ic_<?php echo $grid_item_count; ?> <?php if ($grid_item_count % 2 == 0) { echo 'even'; } else { echo 'odd'; }; ?>">
		<?php
			$header_background_color = get_sub_field('bg_col');

			$hex       = str_replace('#', '', $header_background_color);
			$r         = (hexdec(substr($hex, 0, 2)) / 255);
			$g         = (hexdec(substr($hex, 2, 2)) / 255);
			$b         = (hexdec(substr($hex, 4, 2)) / 255);
			$lightness = round((((max($r, $g, $b) + min($r, $g, $b)) / 2) * 100));
			if ($lightness >= 55) {
				$header_text_colour = '#222';
			} else {
				$header_text_colour = '#eee';
			};

			echo '<dt style="background-color:' . $header_background_color . '; color:' . $header_text_colour . ';">';
	      the_sub_field('table_header');
			echo '</dt>';

			if (have_rows('table_content')) {
				while ( have_rows('table_content') ) : the_row();

					$text_or_image = get_sub_field('text_or_image');
					$cell_style = get_sub_field('style');

					if ($text_or_image == 'text') {
						$text_content = get_sub_field('text_content');
						if ($text_content == "") {
							$text_content = "&nbsp;";
						};
						$cell_content = '<dd class="' . $cell_style. ' text">' . $text_content . '</dd>';
					};

					if ($text_or_image == 'button') {
						$button_link = get_sub_field('button_link');
						$button_text = get_sub_field('button_text');
						if (get_sub_field('button_colour')) {
							$button_colour = get_sub_field('button_colour');
						} else {
							$button_colour  = '#333333';
						};

							$hex       = str_replace('#', '', $button_colour);
							$r         = (hexdec(substr($hex, 0, 2)) / 255);
							$g         = (hexdec(substr($hex, 2, 2)) / 255);
							$b         = (hexdec(substr($hex, 4, 2)) / 255);
							$lightness = round((((max($r, $g, $b) + min($r, $g, $b)) / 2) * 100));
							if ($lightness >= 55) {
								$button_text_colour = '#222';
							} else {
								$button_text_colour = '#eee';
							};

						$cell_content = '<dd class="' . $cell_style. ' button"><a style="background-color:' . $button_colour . '; color:' . $button_text_colour . ';" href="' . $button_link . '">' . $button_text . '</a></dd>';
					};

					echo $cell_content;
				endwhile;
				echo '</dl>';
			};

				++$grid_item_count;
		  endwhile;
	};?>

</div>
</div>
