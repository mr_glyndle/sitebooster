<?php

if( get_row_layout() == 'plain_text' ):
	$slice_type = 'text';
	standardslice();

elseif( get_row_layout() == 'p_grid' ):
	$slice_type = 'grid_cont';
	include(locate_template('partials/profile_grid.php'));

elseif( get_row_layout() == 'recent_grid' ):
	$slice_type = 'grid_cont';
	include(locate_template('partials/recent_grid.php'));

elseif( get_row_layout() == 'two_column' ):
	$slice_type = 'text two_column';
	twocolumn();

elseif( get_row_layout() == 'image_left' ):
	$slice_type = 'text_image';
	$align_image = 'left';
	textandimage();

// Make these reusable functions rather than repeatedly calling files?
elseif( get_row_layout() == 'tabbed_sections' ):
	$slice_type = 'tabbed';
	include(locate_template('partials/tabbed.php'));

elseif( get_row_layout() == 'carousel' ):
	$slice_type = 'carousel';
	include(locate_template('partials/carousel.php'));

elseif( get_row_layout() == 'media_element' ):
	$slice_type = 'external_media';
	include(locate_template('partials/media_element.php'));

elseif( get_row_layout() == 'gallery' ):
	$slice_type = 'gallery text';
	include(locate_template('partials/gallery.php'));

elseif( get_row_layout() == 'slice_library' ):
	$slice_type = 'library';
	include(locate_template('partials/library.php'));

elseif( get_row_layout() == 'feature_list' ):
	$slice_type = 'feat_list';
	include(locate_template('partials/custom_feat_list.php'));

endif;

if ( 'accom' == get_post_type() ) {

	$slice_type = 'feat_list';

	if( get_row_layout() == 'feat_list' ):
		include( locate_template('../../plugins/plugin-accommodation/accom/feat_list.php'));
	elseif( get_row_layout() == 'cust_feat_list' ):
		include( locate_template('../../plugins/plugin-accommodation/accom/custom_feat_list.php'));
	endif;

}

++$item_count;
++$GLOBALS['item_count'];
