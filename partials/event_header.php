<?php
$slice_name = 'event_header';
$txt_col =  'dark';
$background_content = 'image';
$background_image_position = 'fixed';
$post_thumbnail_id = get_post_thumbnail_id( );
function myprefix_tribe_event_featured_image() {
	return false;
}
if( tribe_is_event() && is_single() ) { // Single Events
	add_filter( 'tribe_event_featured_image', 'myprefix_tribe_event_featured_image' );
}
if (get_the_post_thumbnail()) {
	$bgimg_xl = wp_get_attachment_image_url($post_thumbnail_id, 'flex_height_xlarge');
	$bgimg_l = wp_get_attachment_image_url($post_thumbnail_id, 'flex_height_large');
	$bgimg_m = wp_get_attachment_image_url($post_thumbnail_id, 'flex_height_medium');
}
?>
<div <?php if (isset($slice_name)) { echo 'id="' . $slice_name . '"'; };?> class="slice <?php echo ' ' . $txt_col;
	if ( (isset($background_image_position) && $background_image_position !== "scroll") || !isset($background_image_position) ) {
		if ( isset($background_content) && ($background_content == "image" && ($background_image_position !== "scroll" || !isset($background_image_position) ))) {
			echo ' parallax';
		}
	};?> "
	<?php echo '>';
	if ((isset($background_content)) && ($background_content == "image") && (isset($bgimg_xl))) {
		$GLOBALS['footer-css'] .= "#" . $slice_name . " { background-image: url('" . $bgimg_m ."'); } @media screen and (min-width: 989px) { #" . $slice_name . " { background-image: url('" . $bgimg_m ."'); } } @media screen and (min-width: 1280px) { #" . $slice_name . " { background-image: url('" . $bgimg_l . "'); } } @media screen and (min-width: 2000px) { #" . $slice_name . " { background-image: url('" . $bgimg_xl . "'); } }";
	}
?>
