<?php
$animate_when = 'span';
$animate_from = get_sub_field('from');
$animate_to = get_sub_field('to');
$animate_easing = 'linear';
$animate_opacity = get_sub_field('opacity');
$animate_scale = get_sub_field('scale');
$animate_rotate_z = get_sub_field('rotate_z');
$animate_translate_x = get_sub_field('translate_x');
$animate_translate_y = get_sub_field('translate_y');?>

<?php if ($animate_when) { ?>
	data-when="<?php echo $animate_when; ?>"
<?php } ?>
<?php if ($animate_from) { ?>
	data-from="<?php echo $animate_from; ?>"
<?php } ?>
<?php if ($animate_to) { ?>
	data-to="<?php echo $animate_to; ?>"
<?php } ?>
<?php if ($animate_easing) { ?>
	data-easing="<?php echo $animate_easing; ?>"
<?php } ?>
<?php if ($animate_opacity) {
	if ($animate_opacity <= .1 ) {
		$animate_opacity = '0';
	} ?>
	data-opacity="<?php echo $animate_opacity; ?>"
<?php } ?>
<?php if ($animate_scale) { ?>
	data-scale="<?php echo $animate_scale; ?>"
<?php } ?>
<?php if ($animate_rotate_z) { ?>
	data-rotatez="<?php echo $animate_rotate_z; ?>"
<?php } ?>
<?php if ($animate_translate_x) { ?>
	data-translatex="<?php echo $animate_translate_x; ?>"
<?php } ?>
<?php if ($animate_translate_y) { ?>
	data-translatey="<?php echo $animate_translate_y; ?>"
<?php } ?>
