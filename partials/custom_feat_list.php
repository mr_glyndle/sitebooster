<?php

	background('feat_list');
	$alignment_over_background = get_sub_field('bg_align');
	overlay($alignment_over_background);

$intro_text = get_sub_field('intro');
?>

		<?php
		if ($intro_text) {
			echo '<div class="txt_blk intro"><div class="intro">' . $intro_text . '</div></div>';
		};

		// check if the repeater field has rows of data
		if( have_rows('feature_items') ):

			echo '<ul>';
		 	// loop through the rows of data
			while ( have_rows('feature_items') ) : the_row();

				$feature_icon = get_sub_field('icon');
				$feature_title = get_sub_field('feature_title');
				$feature_add_link = get_sub_field('add_link');
				$feature_link = get_sub_field('link');

				if ($feature_icon || $feature_title) {

					if ( $feature_add_link && $feature_link !== '' ) {
						echo '<a href="' . $feature_link . '">';
					} ?>

						<li class="feat_item <?php if($feature_icon) { echo 'icon'; } ?>">
							<?php if ($feature_icon) {
								echo '<img class="feat_icon" src="' .  $feature_icon['sizes']['thumbnail'] . '" alt="' . $feature_icon['alt'] . '" title="' . $feature_icon['title'] . '"/>';
							}
							if($feature_title) {
								echo '<span class="feat_title">' . $feature_title . '</span>';
							}
						echo '</li>';

					if ( $feature_add_link && $feature_link !== '' ) {
						echo '</a>';
					}

				}
		    endwhile;
			echo '</ul>';
		else :
			// no rows found
		endif; ?>
	</div>
</div>
