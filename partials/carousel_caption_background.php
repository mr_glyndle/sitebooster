<?php
	$Hex_color = str_replace("#", "", $caption_background_color);
	if(strlen($Hex_color) == 3) {
		$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
		$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
		$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
	} else {
		$r = hexdec(substr($Hex_color,0,2));
		$g = hexdec(substr($Hex_color,2,2));
		$b = hexdec(substr($Hex_color,4,2));
	}
	$rgb = array($r, $g, $b);
	$Final_Rgb_color = implode(", ", $rgb);
$GLOBALS['footer-css'] .= "#brandchap .carousel .caption.custom" . $carousel_slide_count . " { background : rgba(" . $Final_Rgb_color . ",.5); }";
