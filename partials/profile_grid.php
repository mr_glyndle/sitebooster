<?php
$show_intro = get_sub_field('text_ab');
$show_outro = get_sub_field('text_be');
$date = get_sub_field('date');
$intro = get_sub_field('grid_header');
$outro = get_sub_field('outro');
$profile_grid_content = get_sub_field('grid_cont');
$number_in_grid = count($profile_grid_content);
include(locate_template('partials/grid_items/grid_variables.php'));
background('grid_cont');
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));?>


<div class="s_over <?php echo $container_class;?> <?php if ($text_align) { echo $text_align; }; ?>">
	<div class="contain avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; };?>">

  		<?php if(($intro && !$show_intro) || ($intro && $show_intro == "yes")) { echo '<div class="intro txt_blk">' . $intro . '</div>'; };
			echo '<div class="items ti_'. count($profile_grid_content) . ' '; if ($carousel == 'yes') { echo 'owl-carousel owl-theme '; }; echo '">';

			while ( have_rows('grid_cont') ) : the_row();

				$content_type = get_sub_field('cont_type');

				if ( $content_type == 'custom') {
					include(locate_template('partials/grid_items/custom_content.php'));
				} else if ( $content_type == 'tag' || $content_type == 'cat' || $content_type == 'accom') {
					include(locate_template('partials/grid_items/tag_content.php'));
				} else if ( $content_type == 'post') {
					include(locate_template('partials/grid_items/post_content.php'));
				};
				if ( $image_shape == 'none' && ( ( $profile_grid_title && $profile_grid_link_to !== "" ) && $profile_grid_main == '') ) {
					$make_button = 'true';
				}
				// Don't include empty grid items
				if ($profile_grid_title || $profile_grid_link_to || $profile_grid_main || $profile_grid_image) {?>

				<div class="item ic_<?php echo $grid_item_count; ?> <?php if ($grid_item_count % 2 == 0) { echo 'even'; } else { echo 'odd'; }; if ($make_button == 'true') { echo ' button'; };?>">
					<?php

					$item_over_col = '';
					$item_over_op = '';
					$item_over_col = get_sub_field('item_bg_col');
					$item_over_op = get_sub_field('item_bg_op');

					if ($item_over_col !== '') {

						if ($item_over_op !== '') {
							if ($item_over_op !== '0') {
								$item_over_op = $item_over_op / 100;
							}
						} else {
							$item_over_op = .8;
						}

						$Item_Hex_color = str_replace("#", "", $item_over_col);

						if(strlen($Item_Hex_color) == 3) {
							$item_r = hexdec(substr($Item_Hex_color,0,1).substr($Item_Hex_color,0,1));
							$item_g = hexdec(substr($Item_Hex_color,1,1).substr($Item_Hex_color,1,1));
							$item_b = hexdec(substr($Item_Hex_color,2,1).substr($Item_Hex_color,2,1));
						} else {
							$item_r = hexdec(substr($Item_Hex_color,0,2));
							$item_g = hexdec(substr($Item_Hex_color,2,2));
							$item_b = hexdec(substr($Item_Hex_color,4,2));
						}

						$item_rgb = array($item_r, $item_g, $item_b);
						$item_rgb_color = implode(", ", $item_rgb);
						$item_rgba_colour = 'rgba(' . $item_rgb_color . ',' . $item_over_op . ')';
					} else {
						$item_rgba_colour = '';
					}

					if ( isset($item_rgb_color) && $item_rgb_color !== '' || isset($rgba_colour) && $rgba_colour !== '' ) {
						echo '<div class="txt_bg"></div>';

						if (strpos($container_class, 'no_overlay') !== false) {

							// Grid item is displaying text below image

							if ($item_rgba_colour !== '') {

								// Item uses custom overlay
								$GLOBALS['footer-css'] .= '
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ' .txt_bg { background-color: ' . $item_rgba_colour . '; transition: background-color ease .3s;}
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ':hover .txt_bg { background-color: rgba(' . $item_rgb_color . ',.9);}';

							} else if ($rgba_colour !== '') {

								// Item uses slice wide overlay
								$GLOBALS['footer-css'] .= '
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ' .txt_bg { background-color: ' . $rgba_colour . '; transition: background-color ease .3s;}
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ':hover .txt_bg { background-color: rgba(' . $Rgb_color . ',.9);}';

							}

						} else {

							// Grid item is overlaying text over image

							if ($text_align == 'center' && $overlay) {

								if (isset($item_rgba_colour) && $item_rgba_colour !== '') {

									$GLOBALS['footer-css'] .= '
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background-color: ' . $item_rgba_colour . ';}
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background-color: rgba(' . $item_rgb_color . ',.2);}';

								} else if (isset($rgba_colour) && $rgba_colour !== '') {

									$GLOBALS['footer-css'] .= '
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background-color: ' . $rgba_colour . '; }
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background-color: rgba(' . $Rgb_color . ', .2); }';

								}

							} else {

								// Grid item is left or right aligned, so we need a gradient overlay

								if ($item_rgba_colour !== '') {

									$GLOBALS['footer-css'] .= '
										.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background: linear-gradient(to bottom, rgba(' . $item_rgb_color . ',0) 0, rgba(' . $item_rgb_color . ',1) 100%); }
										.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background: linear-gradient(to bottom, rgba(' . $item_rgb_color . ',0) 0%, rgba(' . $item_rgb_color . ',1 ) 30%); }
									';

								} else if ($rgba_colour !== '') {

									$GLOBALS['footer-css'] .= '
										.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background: linear-gradient(to bottom, rgba(' . $Rgb_color . ',0) 0%, ' . $rgba_colour . ' 100%); }
										.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background: linear-gradient(to bottom, rgba(' . $Rgb_color . ',0) 0%, rgba(' . $Rgb_color . ', 1) 30%); }';

								}

							}

						}

					};?>

				<?php if (isset($make_button) && $make_button == 'true') {
					echo '<h4><a class="btn" href="' . $profile_grid_link_to . '">' . $profile_grid_title . '</a></h4>';
				} else {

					include(locate_template('partials/grid_items/grid_images.php'));

					if ($overlay && $profile_grid_link_to) { ?>

						<a class="text" href="<?php echo $profile_grid_link_to; ?>">
							<div class="text_content">
						        <?php
								echo '<h4>' . $profile_grid_title . '</h4>';
								if ($profile_grid_main !== '<p></p>') {
									echo $profile_grid_main;
								}	?>
					  		</div>
						</a>

						<?php } else {
							if($profile_grid_title || $profile_grid_main) {?>
								<div class="text">
									<div class="text_content">
										<?php
										if ($profile_grid_title) {
											if ($profile_grid_link_to) {
												echo '<h4><a href="' . $profile_grid_link_to . '">' . $profile_grid_title . '</a></h4>';
											} else {
												echo '<h4>' . $profile_grid_title . '</h4>';
											}
										}
										if ($date && $date[0] == '1') {
											echo '<p class="small mod-date">' . $profile_grid_date . '</p>';
										}
										if ($profile_grid_main !== '') {
											echo $profile_grid_main;
										};

										$max_occupancy = '';
										$max_occupancy = get_field('max_occupancy');
										if ($max_occupancy && ($max_occupancy !== '')) {

											$no_beds = get_field('no_beds');
											$no_rooms = get_field('no_rooms');
											$pets = get_field('pets');

											echo '<p class="small tags">';
											if($no_rooms) {
												echo '  <span><svg class="icon icon-single_bed"><use xlink:href="'. $link_to_svg .'#icon-single_bed"></use></svg><b>'.$no_rooms . '</b> bedroom';
												if ($no_rooms > 1) {
													echo 's';
												}
												echo '</span>';
											}
											if ($no_beds) {
												echo '  <span><svg class="icon icon-single_bed"><use xlink:href="'. $link_to_svg .'#icon-single_bed"></use></svg><b>'.$no_beds.'</b> bed';
												if ($no_beds > 1) {
													echo 's';
												}
												echo '</span>';
											}
											if($max_occupancy) {
												echo '  <span><svg class="icon icon-person_outline"><use xlink:href="'. $link_to_svg .'#icon-person_outline"></use></svg>Up to <b>' . $max_occupancy . '</b> guests</span>';
											}
											if ($pets == "True") {
												echo '<span><svg class="icon icon-pets"><use xlink:href="'. $link_to_svg .'#icon-pets"></use></svg> Pet Friendly</span>';
											}

											echo '</p>';


											$categories = get_the_terms( $post->ID, 'accommodation_type' );
											if ($categories) {
												echo '<p class="small tags"><svg class="icon icon-location"><use xlink:href="'. $link_to_svg .'#icon-location"></use></svg>';
												$i = 1;
												$c = count($categories);
												foreach( $categories as $category ) {
													echo $category->name;
													if (($i < $c) && ($c !== 1)) {
														echo ', ';
													}
													++$i;
												}
												echo '</p>';
											}
										}

										if($profile_grid_link_to !== '') {
											echo '<p class="link"><a href="' . $profile_grid_link_to . '" class="btn">More...</a></p>';
										};?>
									</div>
								</div>

							<?php };
						};
				}; ?>
			</div>
		<?php };
		++$grid_item_count;
		wp_reset_postdata();
		endwhile;

	echo '</div>';
	if(($outro && !$show_outro) || ($outro && $show_outro == "yes")) { echo '<div class="outro txt_blk">' . $outro . '</div>'; };
echo '</div>';?>
</div>

<?php if ($carousel == 'yes') {
	include(locate_template('partials/grid_items/carousel_script.php'));
}; ?>
</div><!-- closing section_background -->
