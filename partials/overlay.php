<?php
$rgba_colour = '';
$a = '';
if (get_sub_field('sname')) {
	$rgba_colour = get_sub_field('over_color');
	if (get_sub_field('over_opac')) {
		$a = get_sub_field('over_opac');
	} else {
		$a = .8;
	}
} else if (isset($head_set['over_color'])) {
	$rgba_colour = $head_set['over_color'];
	if (isset($head_set['over_opac'])) {
		$a = $head_set['over_opac'];
	}
} else if (get_field('sname')) {
	$rgba_colour = get_field('over_color');
	if (get_field('over_opac')) {
		$a = get_field('over_opac');
	} else {
		$a = .8;
	}
}
if ($rgba_colour !== '') {
	$Hex_color = str_replace("#", "", $rgba_colour);
	if(strlen($Hex_color) == 3) {
		$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
		$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
		$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
	} else {
		$r = hexdec(substr($Hex_color,0,2));
		$g = hexdec(substr($Hex_color,2,2));
		$b = hexdec(substr($Hex_color,4,2));
	}
	$rgb = array($r, $g, $b);
	$Rgb_color = implode(", ", $rgb);
	$rgba_colour = 'rgba(' . $Rgb_color . ',' . $a . ')';
};
?>
