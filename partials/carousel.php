<?php
$full_width_carousel = get_sub_field('full_width_carousel');
$carousel_slide_count = 0;

$options = get_sub_field_object('options');
$options_value = $options['value'];
$options_choices = $options['choices'];

$timing = get_sub_field('timing');

$options_to_add = '';
if ($options_value):
    foreach ($options_value as $v):
        $options_to_add = $options_to_add.$v.': true,';
    endforeach;
endif;
?>
<div class="owl-carousel carousel owl-theme carousel_<?php echo $item_count; ?> slide <?php if ($full_width_carousel[0] == 'yes') {
    echo 'full';
}; ?>">
    <?php while (have_rows('carousel_slide')) : the_row(); ?>
    <?php
        $carousel_slide_text = get_sub_field('carousel_slide_text');
        $carousel_slide_image = get_sub_field('carousel_slide_image');
        $caption_background_color = get_sub_field('caption_background_color');
        $remove_overlay = get_sub_field('remove_overlay');
        $carousel_slide_text_colour = get_sub_field('carousel_slide_text_colour');
        $carousel_slide_link_url = get_sub_field('carousel_slide_link_url');
        $carousel_slide_link_text = get_sub_field('button_link_text');
		$caption_location  = get_sub_field('caption_location');
    ?>

    <div class="item <?php if ($carousel_slide_count < 1) { echo 'active'; };?>  caption_location_<?php if ( $caption_location ) { echo $caption_location; }; ?>">

		<picture>
			<source
				media="(min-width: 527px)"
				srcset="<?php echo $carousel_slide_image['sizes']['pano_x_large']; ?>  2000w,
				<?php echo $carousel_slide_image['sizes']['pano_x_large']; ?> 1280w,
				<?php echo $carousel_slide_image['sizes']['golden_large']; ?>  898w,
				<?php echo $carousel_slide_image['sizes']['golden_medium']; ?>  527w"
				sizes="100vw" />
			<source
				srcset="<?php echo $carousel_slide_image['sizes']['golden_large']; ?> 2x,
				<?php echo $carousel_slide_image['sizes']['golden_medium']; ?> 1x" />

			<img
				src="<?php echo $carousel_slide_image['sizes']['golden_small']; ?>" <?php if ( $carousel_slide_image['alt'] ) { echo 'alt="' . $carousel_slide_image['alt'] . '"'; } else if ($carousel_slide_text) { echo 'alt="'; echo strip_tags($carousel_slide_text); echo '"'; }; ?>/>
		</picture>

		<?php if ($carousel_slide_text) { ?>

			<div class="caption <?php echo $carousel_slide_text_colour; if ($caption_background_color) { echo ' custom'.$carousel_slide_count; };?>">
				<?php if ($remove_overlay) { $GLOBALS['footer-css'] .= "#brandchap .carousel .caption, #brandchap .carousel .caption { background : transparent; }";
				} else if ($caption_background_color) {
					include locate_template('partials/carousel_caption_background.php');
				}; ?>
			    <div class="cap-cont">
			      <?php if ($carousel_slide_text) {
					  echo $carousel_slide_text;
				  };
				  if ($carousel_slide_link_url) {
					  if ($carousel_slide_link_text) {
						  echo '<a class="btn" href="' . $carousel_slide_link_url . '">' . $carousel_slide_link_text . '</a>';
					  };
				  };?>
			    </div>
			</div>

		  <?php }; ?>

    </div>

  <?php ++$carousel_slide_count;
  endwhile; ?>
</div>

<?php $GLOBALS['footer-js'] = $GLOBALS["footer-js"] . "jQuery('.carousel_" . $item_count . ".owl-carousel').owlCarousel({loop:true, animateOut: 'fadeOut', animateIn: 'fadeIn',dots:true,autoplay:true,lazyLoad:true,autoplayTimeout:" . $timing . ",autoplayHoverPause:true,items:1,animateOut: 'fadeOut'});" ?>
