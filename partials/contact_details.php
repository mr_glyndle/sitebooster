<?php $phone = get_field('phone_number', 'option'); $business_name = get_field('business_name', 'option'); $address = get_field('address', 'option'); $address_town = get_field('address_town', 'option'); $address_county = get_field('address_county', 'option'); $postcode = get_field('postcode', 'option'); $address_country = get_field('address_country', 'option'); $business_desc = get_field('business_desc', 'option'); ?>

<?php if ($business_name || $phone || $address || $postcode) { ?>

<div class="inline-block vtop left">

		<?php if ($business_name) { ?>
			<p class="businessname"><b><?php echo $business_name; ?></b></p>
		<?php } else { ?>
			<p class="businessname"><b><?php echo bloginfo( 'name' ); ?></b></p>
		<?php }?>
		<?php if ($address || $address_town || $address_county || $postcode || $address_country) {
			echo '<p>';
			if (isset($address)) { echo $address; }
			if (isset($address_town)) { echo ', ' . $address_town; }
			if (isset($address_county)) { echo ', ' . $address_county; }
			if (isset($postcode)) { echo ', ' . $postcode; }
			if (isset($address_country)) { echo ', ' . $address_country; }
		};

		if ($phone) { ?><span class="telephone"> - <a href="tel:<?php echo str_replace(' ', '', $phone); ?>"><?php echo $phone; ?></a></span><?php }; ?>

		</p>

	<?php if (isset($business_desc)) {
		echo '<p>' . $business_desc . '</p>';
	};

	include(locate_template('partials/social_links.php')); ?>


</div>

<?php }; ?>
