<?php unset($add_vertical_space);
$alignment_over_background = get_sub_field('bg_align');
$remove_vertical_space = get_sub_field_object('v_space');
$remove_vertical_space_value = $remove_vertical_space['value'];
$remove_vertical_space_choices = $remove_vertical_space['choices'];
$space_to_remove = '';
if( $remove_vertical_space_value ):
	foreach( $remove_vertical_space_value as $v ):
		$space_to_remove = $space_to_remove . 'nopad_' . $v . ' ';
	endforeach;
endif;
$add_vertical_space = get_sub_field('add_space');
$add_vertical_margin = get_sub_field('add_space_ext');
$background_content = get_sub_field('bg_cont');
