<?php
if ($slice_name) {
	$slice_name = preg_replace("/[^A-Za-z0-9 ]/", '', $slice_name);
	$slice_name = str_replace(' ', '_', $slice_name);
};
$jumbo_text = get_sub_field('jumbotron_text');
$jumbo_image = get_sub_field('jumbotron_background_image');
$jumbo_text_colour = get_sub_field('text_colour');
$jumbo_link_url = get_sub_field('link_url');
$jumbo_link_text = get_sub_field('link_button_text');
$jumbo_height = get_sub_field('height');
$animate = get_sub_field('animate');
$caption_location = get_sub_field('caption_location');
$caption_background_color = get_sub_field('caption_background_color');
$remove_overlay = get_sub_field('remove_overlay');
if ($animate) {
	$animate = $animate[0];
};?>
<div <?php if ($slice_name) { echo 'id="' . $slice_name . '"'; };?> class="jumbotron jumbotron_<?php echo $item_count; ?> text-center scrollme background  height-<?php if ( $jumbo_height == 'one_third' ) { echo 'one_third'; } else if ( $jumbo_height == 'two_thirds' ) { echo 'two_thirds'; } else if ( $jumbo_height == 'full' ){ echo 'full'; };?> caption_location_<?php if ( $caption_location ) { echo $caption_location; }; ?> <?php if ($animate) { echo 'scrollme'; };?> ">


	<div class="background-image <?php if ($animate) { echo 'animateme'; };?>" <?php if ($animate) { include(locate_template('partials/animate.php')); }; ?> >
		<img srcset="<?php echo $jumbo_image['sizes']['flex_height_xxlarge']; ?>  3000w,
		<?php echo $jumbo_image['sizes']['flex_height_xlarge']; ?>  2000w,
		<?php echo $jumbo_image['sizes']['flex_height_large']; ?>  1280w,
		<?php echo $jumbo_image['sizes']['flex_height_medium']; ?>   898w"
		sizes="100vw"
		src="<?php echo $jumbo_image['sizes']['flex_height_small']; ?>"
		class="lazyload"
		alt="<?php echo $jumbo_image['alt']?>"
		title="<?php echo $jumbo_image['title']?>">
	</div>

	<div class="caption <?php echo $jumbo_text_colour; if ($caption_background_color) { echo ' custom' . $item_count; }; ?>">
		<div class="cap-cont"><?php echo $jumbo_text; ?><?php if ($jumbo_link_url && $jumbo_link_text) { ?><a class="btn" href="<?php echo $jumbo_link_url; ?>"><?php echo $jumbo_link_text; ?></a><?php }; ?></div>

		<?php
		if ($remove_overlay) {
			if ($remove_overlay[0] == 'remove') {
				$GLOBALS['footer-css'] .= ".caption.custom" . $item_count . "{ background : transparent; }";
			}
		} else if ($caption_background_color && !$remove_overlay) {
			include(locate_template('partials/caption_background.php'));
		};?>
	</div>
</div>
