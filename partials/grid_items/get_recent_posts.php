<?php
	$get_posts_from_type = get_sub_field('get_posts_from');

	if (current_user_can('edit_others_posts')) {
		$get_post_status = 'any';
	} else {
		$get_post_status = 'publish';
	}

	// GET POSTS FROM CATEGORY OR TAG
	if ($get_posts_from_type !== 'child') {

		if ($get_posts_from_type == 'category') {
			$post_type = 'post';
			$taxonomy = 'category';
	    $get_posts_from_ID = get_sub_field('posts_from_category');

		} elseif ($get_posts_from_type == 'tag') {
			$post_type = 'post';
			$taxonomy = 'post_tag';
		  $get_posts_from_ID = get_sub_field('posts_from_tag');

		} elseif ($get_posts_from_type == 'tribe_events_cat') {
			$post_type = 'tribe_events';
			$taxonomy = 'tribe_events_cat';
			$get_posts_from_ID = get_sub_field('posts_from_event_category');

		} elseif ($get_posts_from_type == 'product_cat') {
			$post_type = 'product';
			$taxonomy = 'product_cat';
	    $get_posts_from_ID = array(get_sub_field('posts_from_product_category'));

		} elseif ($get_posts_from_type == 'product_tag') {
			$post_type = 'product';
			$taxonomy = 'product_tag';
	    $get_posts_from_ID = get_sub_field('posts_from_product_tag');

		} elseif ($get_posts_from_type == 'accom') {
			$post_type = 'accom';
			$taxonomy = 'accommodation_type';
			$get_posts_from_ID = get_sub_field('posts_from_accom_category');

		} elseif ($get_posts_from_type == 'accom_f') {
			$post_type = 'accom';
			$taxonomy = 'accommodation_feature';
			$get_posts_from_ID = get_sub_field('posts_from_accom_feat');

		} elseif ($get_posts_from_type == 'filter') {
			$post_type = 'filterable';
			$taxonomy = 'accommodation_feature';
			$get_posts_from_ID = get_sub_field('posts_from_filter_category');
		}

		$args = array(
			'posts_per_page'	=> $number_in_grid,
			'post_status' 		=> $get_post_status,
      'orderby'					=> $order_by,
      'order' 					=> $order,
      'post_type' 			=> $post_type,
			'tax_query'       => array(
				array(
					'taxonomy'					=> $taxonomy,
					'field'    					=> 'term_id',
					'terms'							=> $get_posts_from_ID,
					'include_children '	=> 1,
					'operator' 					=> 'IN',
				)
			)
		);

	// GET CHILD PAGES OF PARENT
	} elseif ($get_posts_from_type == 'child') {

		$get_posts_from_ID = get_sub_field('parent');

		if(isset($get_posts_from_ID)) {
			$get_posts_from_ID = $get_posts_from_ID[0];
		}

		$args = array(
			'post_parent' => $get_posts_from_ID,
			'post_type'   => 'page',
			'exclude'     => $post->ID,
			'numberposts' => $number_in_grid,
			'post_status' => $get_post_status,
      'orderby' 		=> $order_by,
      'order' 			=> $order,
		);

	}
?>
