<?php
if($full_width == 'contained') {
	$GLOBALS['footer-js'] = $GLOBALS['footer-js'] . "jQuery('.slice_" . $item_count . " .items').owlCarousel({loop:true,center: true,nav:true,dots:false,navText:['&lt;','&gt;'],autoplay:true,lazyLoad:true,autoplayTimeout:2500,autoplayHoverPause:true,responsive:{0:{items:1},600:{items:2},972:{items:3}}});";
} else {
	$GLOBALS['footer-js'] = $GLOBALS['footer-js'] . "jQuery('.slice_" . $item_count . " .items').owlCarousel({loop:true,center: true,stagePadding: 30,nav:true,dots:false,navText:['&lt;','&gt;'],autoplay:true,lazyLoad:true,autoplayTimeout:2500,autoplayHoverPause:true,responsive:{0:{items:1},600:{items:2},972:{items:3},1600:{items:4},2200:{items:5},2800:{items:6}}});";
};
