<?php
	$link_post_ID = get_sub_field('post_link');
	$post = get_post( $link_post_ID );

	$profile_grid_title = get_the_title();
	if (get_post_status() !== 'publish' && get_post_status() !== 'private') {
		$profile_grid_title = ucfirst(get_post_status()) . ': ' . $profile_grid_title;
	}
	$profile_grid_main = '';
	if (get_the_excerpt()) {
		$profile_grid_main = '<p>' . get_excerpt(160) . '</p>';
	}
	$profile_grid_date = get_the_modified_date();

	$profile_grid_image = wp_get_attachment_image_src( get_post_thumbnail_id(), $image_shape );

	$profile_grid_image_width = $profile_grid_image[1];
	$profile_grid_image_height = $profile_grid_image[2];
	$profile_grid_image = $profile_grid_image[0];

	$profile_grid_image_small = wp_get_attachment_image_src( get_post_thumbnail_id(), 'pano_small' );
	$profile_grid_image_small_width = $profile_grid_image_small[1];
	$profile_grid_image_small_height = $profile_grid_image_small[2];
	$profile_grid_image_small = $profile_grid_image_small[0];

	$profile_grid_image_small_x2 = wp_get_attachment_image_src( get_post_thumbnail_id(), 'pano_medium' );
	$profile_grid_image_small_x2_width = $profile_grid_image_small_x2[1];
	$profile_grid_image_small_x2_height = $profile_grid_image_small_x2[2];
	$profile_grid_image_small_x2 = $profile_grid_image_small_x2[0];

	$profile_grid_image_original_small = wp_get_attachment_image_src( get_post_thumbnail_id(), 'flex_height_small');
	$profile_grid_image_original_small_width = $profile_grid_image_original_small[1];
	$profile_grid_image_original_small_height = $profile_grid_image_original_small[2];
	$profile_grid_image_original_small = $profile_grid_image_original_small[0];

	$profile_grid_image_original_small_x2 = wp_get_attachment_image_src( get_post_thumbnail_id(), 'flex_height_medium');
	$profile_grid_image_original_small_x2_width = $profile_grid_image_original_small_x2[1];
	$profile_grid_image_original_small_x2_height = $profile_grid_image_original_small_x2[2];
	$profile_grid_image_original_small_x2 = $profile_grid_image_original_small_x2[0];

	$profile_grid_link_to = get_permalink();
?>
