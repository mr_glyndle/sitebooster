<?php
$profile_grid_main = '';
$profile_grid_image = '';

$profile_grid_title = get_sub_field('p_grid_title');
$profile_grid_main = get_sub_field('p_grid_main');
$profile_grid_image_source = get_sub_field('p_grid_image');

if ($profile_grid_image_source && $image_shape !== 'none') {

	$profile_grid_image = $profile_grid_image_source['sizes'][$image_shape];
	$profile_grid_image_width = $profile_grid_image_source['sizes'][$image_shape . '-width'];
	$profile_grid_image_height = $profile_grid_image_source['sizes'][$image_shape . '-height'];

	$profile_grid_image_small = $profile_grid_image_source['sizes']['pano_small'];
	$profile_grid_image_small_width = $profile_grid_image_source['sizes']['pano_small-width'];
	$profile_grid_image_small_height = $profile_grid_image_source['sizes']['pano_small-height'];

	$profile_grid_image_small_x2 = $profile_grid_image_source['sizes']['pano_medium'];
	$profile_grid_image_small_x2_width = $profile_grid_image_source['sizes']['pano_medium-width'];
	$profile_grid_image_small_x2_height = $profile_grid_image_source['sizes']['pano_medium-height'];

	$profile_grid_image_original_small = $profile_grid_image_source['sizes']['flex_height_small'];
	$profile_grid_image_original_small_width = $profile_grid_image_source['sizes']['flex_height_small-width'];
	$profile_grid_image_original_small_height = $profile_grid_image_source['sizes']['flex_height_small-height'];

	$profile_grid_image_original_small_x2 = $profile_grid_image_source['sizes']['flex_height_medium'];
	$profile_grid_image_original_small_x2_width = $profile_grid_image_source['sizes']['flex_height_medium-width'];
	$profile_grid_image_original_small_x2_height = $profile_grid_image_source['sizes']['flex_height_medium-height'];
}

$profile_grid_link_type = get_sub_field('p_grid_btn_link');

if ($profile_grid_link_type == 'external') {
	$profile_grid_link_to = get_sub_field('p_grid_link_to');

} else if ($profile_grid_link_type == 'internal') {
	$profile_grid_link_to = get_sub_field('p_grid_int_link');
	$profile_grid_link_to = $profile_grid_link_to[0];
	$profile_grid_link_to = get_permalink ($profile_grid_link_to);

} else if ($profile_grid_link_type == 'cat') {
	$profile_grid_link_to = get_sub_field('custom_category_link');
	$profile_grid_link_to = get_category_link($profile_grid_link_to, 'category');

} else if ($profile_grid_link_type == 'tag') {
	$profile_grid_link_to = get_sub_field('custom_tag_link');
	$profile_grid_link_to = get_tag_link($profile_grid_link_to);

} else if ($profile_grid_link_type == 'accom') {
	$term_id = get_sub_field('accom_type');
	$profile_grid_link_to = get_term_link( $term_id , 'accommodation_type' );
};
$profile_grid_button_text = get_sub_field('p_grid_button_text');


$make_button = '';

if ($profile_grid_title && $profile_grid_link_to) {

	if ($profile_grid_main) {

	} else {

		if (!$profile_grid_image || $image_shape == 'none') {
			$make_button = 'true';
		}
	}
};
?>
