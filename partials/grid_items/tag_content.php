<?php
if ( $content_type == 'tag') {
	$term = get_sub_field('tag_link');
	$term = get_term( $term, 'post_tag' );
} else if ($content_type == 'cat') {
	$term = get_sub_field('category_link');
	$term = get_term( $term, 'category' );
} else if ($content_type == 'accom') {
	$term = get_sub_field('accom_link');
	$term = get_term( $term, 'accommodation_type' );
};
if( $term ):
	$profile_grid_title = $term->name;
	$profile_grid_main = '<p>' . $term->description . '</p>';

	$profile_grid_image_source = get_field('category_image', $term);

	$profile_grid_image = $profile_grid_image_source['sizes'][$image_shape];
	$profile_grid_image_small = $profile_grid_image_source['sizes']['pano_small'];
	$profile_grid_image_small_x2 = $profile_grid_image_source['sizes']['pano_medium'];


	$profile_grid_image_original_small = $profile_grid_image_source['sizes']['flex_height_small'];
	$profile_grid_image_original_small_x2 = $profile_grid_image_source['sizes']['flex_height_medium'];

	$profile_grid_link_to = get_term_link( $term );
endif; ?>
