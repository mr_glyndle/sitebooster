<?php
$profile_grid_image_shape = get_sub_field('p_grid_img_shape');

$grid_item_count = 1;
$text_align = get_sub_field('text_align');
$make_button = '';

$container_class = 'grid responsive ti_' . $number_in_grid . ' image_type_' . $profile_grid_image_shape;

if(get_sub_field('overlay_text_on_image')){
	$overlay = get_sub_field('overlay_text_on_image');
	$container_class .= ' overlay';
} else {
	$overlay = '';
	$container_class .= ' card no_overlay';
};
$carousel = '';

if (get_sub_field('make_carousel')) {
	$carousel = get_sub_field('make_carousel');
	if ($carousel[0] == 'yes') {
		$carousel = 'yes';
		$container_class .= ' carousel';
	};
};

if (get_sub_field('p_grid_full_scrn')) {
	$full_width = get_sub_field('p_grid_full_scrn');
	$full_width = $full_width[0];
	if ($full_width == 'yes') {
		$container_class .= ' full';
	}
} else {
	$full_width = 'contained';
	$container_class .= ' contained';
}

if ($number_in_grid % 2 == 0) {
	$odd_or_even = 'even';
} else {
	$odd_or_even = 'odd';
};
$container_class .= ' ' . $odd_or_even;
if ($profile_grid_image_shape == 'default') {
	$image_shape = 'square';
} if ($profile_grid_image_shape == 'original') {
	$image_shape = 'flex_height_small';
} if ($profile_grid_image_shape == 'pano_small') {
	$image_shape = 'pano_small';
} if ($profile_grid_image_shape == 'none') {
	$image_shape = 'none';
} else if ($profile_grid_image_shape == 'golden_small') {
	$image_shape = 'golden_small';
};?>
