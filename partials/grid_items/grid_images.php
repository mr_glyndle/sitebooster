<?php
if($image_shape !== 'none') {
	if (!$profile_grid_image) {

		$profile_grid_image = get_template_directory_uri() . '/img/placeholder_' . $image_shape . '.png';
		$profile_grid_image_width = '527';
		$profile_grid_image_height = '371';

		$profile_grid_image_small = get_template_directory_uri() . '/img/placeholder_pano_small.png';
		$profile_grid_image_small_x2 = get_template_directory_uri() . '/img/placeholder_pano_small.png';
		$profile_grid_image_original_small = get_template_directory_uri() . '/img/placeholder_flex_height_small.png';
		$profile_grid_image_original_small_x2 = get_template_directory_uri() . '/img/placeholder_flex_height_small.png';

	}
	if ($profile_grid_link_to) {
		echo '<a href="' . $profile_grid_link_to . '" class="image_container">';
		} else {
		echo '<div class="image_container">';
	};

	if ($grid_item_count == '1' && ($image_shape !== 'flex_height_small' && $image_shape !== 'square')) {
		if ($carousel !== 'yes') {
			if ($odd_or_even == 'odd') { ?>



				<?php if( strpos( $profile_grid_image, '.svg' ) !== false) {
					$svg_content = file_get_contents_ssl( $profile_grid_image );
					echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
				} else { ?>

					<picture>
					<!-- Show chosen image size above 780px width -->
					<source 	data-srcset=	"<?php echo $profile_grid_image; ?>"
								media=			"(min-width: 780px)"/>

					<!-- Show panoramic image and stack vertically when below 779px width -->
					<source 	media=			"(min-width: 560px)"
								data-srcset=	"<?php echo $profile_grid_image_small_x2; ?>" />

					<!-- Show panoramic image and stack vertically when below 370px width -->
					<source 	media=			"(max-width: 370px)"
								data-srcset=	"<?php echo $profile_grid_image; ?>" />

					<img	<?php if ($carousel !== 'yes') {
								echo 'class="lazyload" ';
								echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
								if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
							} else {
								echo " src='" . $profile_grid_image . "'";
							};
							if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "' alt='" . $profile_grid_title . "'"; };
							if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
							if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };?>
						/>

					</picture>
					<?php } ?>



			<?php

		} else if ($odd_or_even == 'even') { ?>
			<?php if( strpos( $profile_grid_image, '.svg' ) !== false) {
				$svg_content = file_get_contents_ssl( $profile_grid_image );
				echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
			} else { ?>
				<img	<?php if($carousel !== 'yes') {
							echo 'class="lazyload" ';
							echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
							if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
						} else {
							if(isset($profile_grid_image)) { echo " src='" . $profile_grid_image . "'"; };
						};
						if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "' alt='" . $profile_grid_title . "'"; };
						if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
						if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };?>
					/>

			<?php }
			}

		} else { ?>

			<?php if( strpos( $profile_grid_image, '.svg' ) !== false) {
				$svg_content = file_get_contents_ssl( $profile_grid_image );
				echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
			} else { ?>

			<img	<?php if($carousel !== 'yes') {
						echo 'class="lazyload" ';
							echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
							if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
						} else {
							if(isset($profile_grid_image)) { echo " src='" . $profile_grid_image . "'"; };
						}
						if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "' alt='" . $profile_grid_title . "'"; };
						if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
						if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };
						?>/>

		<?php }
		}

	} else if ($grid_item_count == '1' && ($image_shape == 'square')) { ?>
		<?php if( strpos( $profile_grid_image, '.svg' ) !== false) {
			$svg_content = file_get_contents_ssl( $profile_grid_image );
			echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
		} else { ?>
			<img	<?php if($carousel !== 'yes') {
						echo 'class="lazyload" ';
						echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
						if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
					} else {
						if(isset($profile_grid_image)) { echo " src='" . $profile_grid_image . "'"; };
					};
					if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "' alt='" . $profile_grid_title . "'"; };
					if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
					if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };?>
				/>

	<?php }
	}

	// Flex height
	if ($image_shape == 'flex_height_small') { ?>
		<?php if( strpos( $profile_grid_image, '.svg' ) !== false) {
			$svg_content = file_get_contents_ssl( $profile_grid_image );
			echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
		} else { ?>
			<picture>
				<!-- Show panoramic image and stack vertically when below 992px width -->
				<source	media=			"(max-width: 992px)"
						data-srcset=	"<?php echo $profile_grid_image_original_small; ?>" />

				<!-- Show chosen image size above 992px width -->
				<source	data-srcset=	"<?php echo $profile_grid_image_original_small_x2; ?>"
						media=			"(min-width: 992px)"/>

				<img	<?php if($carousel !== 'yes') {
							echo 'class="lazyload" ';
							echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
							if(isset($profile_grid_image_original_small)) { echo " data-src='" . $profile_grid_image_original_small . "'"; };
						} else {
							if(isset($profile_grid_image_original_small)) { echo " src='" . $profile_grid_image_original_small . "'"; };
						}
						if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "' alt='" . $profile_grid_title . "'"; };
						if(isset($profile_grid_image_original_small_width)) { echo " width='" . $profile_grid_image_original_small_width . "'"; };
						if(isset($profile_grid_image_original_small_height)) { echo " height='" . $profile_grid_image_original_small_height . "'"; };
						?>/>
			</picture>
	<?php } ?>
	<!-- Not first item and not flex height -->
	<?php } else if ($grid_item_count !== 1 && $image_shape !== 'flex_height_small') {

		if( strpos( $profile_grid_image, '.svg' ) !== false) {
			$svg_content = file_get_contents_ssl( $profile_grid_image );
			echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
		} else {

		if ($carousel !== 'yes') { ?>

				<img <?php if($carousel !== 'yes') {
							echo 'class="lazyload" ';
							echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
							if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
						} else {
							if(isset($profile_grid_image)) { echo " src='" . $profile_grid_image . "'"; };
						}
						if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "'"; };
						if(isset($profile_grid_title)) { echo " alt='" . $profile_grid_title . "'"; };
						if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
						if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };
						?>/>

	<?php } else { ?>

		<img 	<?php if($carousel !== 'yes') {
					echo 'class="lazyload" ';
					echo 'src= "data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="';
					if(isset($profile_grid_image)) { echo " data-src='" . $profile_grid_image . "'"; };
				} else {
					if(isset($profile_grid_image)) { echo " src='" . $profile_grid_image . "'"; };
				}
				if(isset($profile_grid_title)) { echo " title='" . $profile_grid_title . "'"; };
				if(isset($profile_grid_title)) { echo " alt='" . $profile_grid_title . "'"; };
				if(isset($profile_grid_image_width)) { echo " width='" . $profile_grid_image_width . "'"; };
				if(isset($profile_grid_image_height)) { echo " height='" . $profile_grid_image_height . "'"; };
				?>/>

   <?php }
 		}
	}
	include(locate_template('partials/category/check_tags.php'));


	$min_price = get_field('min-price');
	if ($min_price) {
		echo '<p class="small price"><span class="from">From</span><br><span class="cost">£' . $min_price . '</span><br><span class="time">Per week</span></p>';
	};

	if ($profile_grid_link_to) {
		echo '</a>';
	} else {
		echo '</div>';
	};
} ?>
