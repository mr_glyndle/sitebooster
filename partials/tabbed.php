<?php
	include(locate_template('partials/section_background.php'));
	include(locate_template('partials/overlay.php'));
	include(locate_template('partials/spacing.php'));
	$tab_count = 0;
?>
<div class="content s_over avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($alignment_over_background) { echo ' ' . $alignment_over_background; }; ?>">

	<ul class="tabs">
		<?php while ( have_rows('tab') ) : the_row(); ?>
			<?php
			$tab_name = get_sub_field('tab_name');
			$tab_name_ID = str_replace(' ', '', $tab_name);
			$tab_name_ID = preg_replace('/[^a-z]+/i', '', $tab_name_ID);
			?>
		  <li><a href="#<?php echo $slice_name; ?>" <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?> class="tab-link desk <?php if ($tab_count == 0) { ?>current <?php }; ?>" data-tab="#<?php echo $tab_name_ID . $tab_count; ?>"><?php echo $tab_name; ?></a></li>
		<?php
			++$tab_count;
			endwhile;
		?>
	</ul>

		<?php $tab_count = 0;
		while ( have_rows('tab') ) : the_row();
		$tab_name = get_sub_field('tab_name');
		$tab_name_ID = str_replace(' ', '', $tab_name);
		$tab_name_ID = preg_replace('/[^a-z]+/i', '', $tab_name_ID);
		$tab_content = get_sub_field('tab_content'); ?>

		<a id="<?php echo $tab_name_ID; ?>" <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?> href="#<?php echo $tab_name_ID; ?>" class="tab-link btn mobile <?php if ($tab_count == 0) { ?>current <?php }; ?>" data-tab="#<?php echo $tab_name_ID . $tab_count; ?>"><?php echo $tab_name; ?></a>

		<div <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?> id="<?php echo $tab_name_ID . $tab_count; ?>" class="tab-content clearfix <?php if ($tab_count == 0) { ?>current <?php }; ?>">
			<?php echo $tab_content ?>
		</div>

		<?php ++$tab_count;
		endwhile; ?>

	</div>
</div>

<?php

$GLOBALS['footer-js'] .= '
jQuery(".slice.slice_' . $GLOBALS['item_count'] . ' a.tab-link").click(function() { var tab_id = jQuery(this).attr("data-tab"); 
jQuery(".slice.slice_' . $GLOBALS['item_count'] . ' a.tab-link").removeClass("current");
jQuery(".slice.slice_' . $GLOBALS['item_count'] . ' .tab-content").removeClass("current");
jQuery(this).addClass("current");
jQuery(tab_id).addClass("current");
return false;
});';
