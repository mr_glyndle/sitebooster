<?php
include(locate_template('partials/section_background.php'));
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));
$intro = get_sub_field('media_header');
$outro = get_sub_field('outro');
$show_intro = get_sub_field('text_ab');
$show_outro = get_sub_field('text_be');
?>

	<div <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?>class="s_over">

		<div class='media avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; ?>'>
			<div class="media-container">
			<?php

			if(($intro && !$show_intro) || ($intro && $show_intro == "yes")) { echo '<div class="intro txt_blk">' . $intro . '</div>'; }


			$video_url = get_sub_field('media_url');
			if (strpos($video_url, 'youtube') !== false) {
				$yt_id = explode('embed/',$video_url);
				$yt_id = $yt_id[1];
				$yt_id = explode("?",$yt_id);
				$yt_id = $yt_id[0];
				$yt_div_id = preg_replace('/[^a-z]/i','', $yt_id);
			?>

				<div class="youtube_container embed-container">
					<div id="yt_<?php echo $yt_div_id; ?>"></div>


					<?php if (!isset($GLOBALS['youtubeAPI'])) {
						$GLOBALS['youtubeAPI'] = "
						var tag = document.createElement('script');
						tag.src = 'https://www.youtube.com/iframe_api';
						var firstScriptTag = document.getElementsByTagName('script')[0];
						firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
						var player;
						function onYouTubeIframeAPIReady() {";
					};

						$GLOBALS['youtubeAPI'] .= "player" . $yt_div_id . " = new YT.Player('yt_" . $yt_div_id . "', {
							id: 'yt_" . $yt_id . "',
							height: '100%',
							width: '100%',
							playerVars: {
								controls: 1,
								showinfo: 0,
								autohide: 1,
								modestbranding: 1,
								iv_load_policy: 3,
								rel: 0,
							},
							videoId: '" . $yt_id . "'

						});"; ?>


				</div>

			<?php } else { ?>
			<div class="media-element">

			  <div class="embed-container">

					<?php echo $video_url;?>

			  </div>

			</div>
			<?php } ?>

			<?php if(($outro && !$show_outro) || ($outro && $show_outro == "yes")) { echo '<div class="outro txt_blk">' . $outro . '</div>'; };?>
			</div>
		</div>

	</div>
</div>
