<?php
$link_to_svg = get_template_directory_uri () . '/img/symbol-defs.svg';

$facebook = get_field('facebook', 'option');
$twitter = get_field('twitter', 'option');
$google = get_field('google', 'option');
$linkedin = get_field('linkedin', 'option');
$instagram = get_field('instagram', 'option');
$youtube = get_field('youtube', 'option');
$vimeo = get_field('vimeo', 'option');
$pinterest = get_field('pinterest', 'option');
$tumblr = get_field('tumblr', 'option');
$flickr = get_field('flickr', 'option');
if ($facebook || $twitter || $google || $linkedin || $instagram || $youtube || $vimeo || $pinterest || $tumblr || $flickr) { ?>

<div class="inline-block vtop social justify-item ">
<?php if ($linkedin) { ?>
	<div class="linkedin"><a target="_blank" rel="noopener" title="Find us on Linkedin" href="<?php echo $linkedin; ?>"><svg class="icon icon-linkedin"><use xlink:href="<?php echo $link_to_svg; ?>#icon-linkedin"></use></svg></a></div>
<?php }; ?>
<?php if ($twitter) { ?>
	<div class="twitter"><a target="_blank" rel="noopener" title="Find us on Twitter" href="<?php echo $twitter; ?>"><svg class="icon icon-twitter"><use xlink:href="<?php echo $link_to_svg; ?>#icon-twitter"></use></svg></a></div>
<?php }; ?>
<?php if ($facebook) { ?>
	<div class="facebook"><a target="_blank" rel="noopener" title="Find us on Facebook" href="<?php echo $facebook; ?>"><svg class="icon icon-facebook"><use xlink:href="<?php echo $link_to_svg; ?>#icon-facebook"></use></svg></a></div>
<?php }; ?>
<?php if ($google) { ?>
	<div class="google"><a target="_blank" rel="noopener" title="Find us on Google" href="<?php echo $google; ?>"><svg class="icon icon-google"><use xlink:href="<?php echo $link_to_svg; ?>#icon-google"></use></svg></a></div>
<?php }; ?>
<?php if ($instagram) { ?>
	<div class="instagram"><a target="_blank" rel="noopener" title="Find us on Instagram" href="<?php echo $instagram; ?>"><svg class="icon icon-instagram"><use xlink:href="<?php echo $link_to_svg; ?>#icon-instagram"></use></svg></a></div>
<?php }; ?>
<?php if ($flickr) { ?>
	<div class="flickr"><a target="_blank" rel="noopener" title="Find us on Flickr" href="<?php echo $flickr; ?>"><svg class="icon icon-flickr"><use xlink:href="<?php echo $link_to_svg; ?>#icon-flickr"></use></svg></a></div>
<?php }; ?>
<?php if ($youtube) { ?>
	<div class="youtube"><a target="_blank" rel="noopener" title="Find us on YouTube" href="<?php echo $youtube; ?>"><svg class="icon icon-youtube"><use xlink:href="<?php echo $link_to_svg; ?>#icon-youtube"></use></svg></a></div>
<?php }; ?>
<?php if ($vimeo) { ?>
	<div class="vimeo"><a target="_blank" rel="noopener" title="Find us on Vimeo" href="<?php echo $vimeo; ?>"><svg class="icon icon-vimeo"><use xlink:href="<?php echo $link_to_svg; ?>#icon-vimeo"></use></svg></a></div>
<?php }; ?>
<?php if ($pinterest) { ?>
	<div class="pinterest"><a target="_blank" rel="noopener" title="Find us on Pinterest" href="<?php echo $pinterest; ?>"><svg class="icon icon-pinterest"><use xlink:href="<?php echo $link_to_svg; ?>#icon-pinterest"></use></svg></a></div>
<?php }; ?>
<?php if ($tumblr) { ?>
	<div class="tumblr"><a target="_blank" rel="noopener" title="Find us on Tumblr" href="<?php echo $tumblr; ?>"><svg class="icon icon-tumblr"><use xlink:href="<?php echo $link_to_svg; ?>#icon-tumblr"></use></svg></a></div>
<?php }; ?>
</div>
<?php }; ?>
