<nav class="filters">
	<ul class="inline-block vmiddle filter menu">
		<li class="menu-item-has-children dropdown"><a class="btn cat" href="#category">Change category</a>
			<ul class="sub-menu">
				<?php wp_list_categories(array('title_li' => '')); ?>
			</ul>
		</li>
	</ul>

	<?php
	$tag_ids = array();
	while (have_posts()) : the_post();
		global $post;
		$newValues = wp_get_post_tags( $post->ID, array( 'fields' => 'ids' ) );
		$newValues = array_diff($newValues, $tag_ids);
		$tag_ids = array_merge($tag_ids, $newValues);
	endwhile;
	$all_tag_ids = $tag_ids;
	if ( !empty($tag_ids[0]) ) {

		$tag_ids = implode (", ", $tag_ids);

		echo '<p class="inline-block vmiddle filter ">Filter by: ';
		$tags = get_tags(array('orderby' => 'count', 'order' => 'DESC', 'include' => $tag_ids));

		$url = $_SERVER["REQUEST_URI"];
		$url = (string)$url;

		$prefix = "?tag=";
		$index = strpos($url, $prefix) + strlen($prefix);
		$current_tag = substr($url, $index);

		$url_parts = explode($prefix, $url);
		$url_before_tag = $url_parts[0];
		if (count($url_parts) > 1) {
			$url_after_tag = $url_parts[1];
		} else {
			$url_after_tag = '';
		};

		$current_tags = explode('+', $url_after_tag);
		$url_after_tag = $prefix . $url_after_tag;

		if (strpos($url, '?tag=') !== false) {

			foreach ((array) $tags as $tag) {

				$this_tag = ($tag->slug);

				if (in_array($this_tag, $current_tags)) {

					$tag_name = $tag->slug;
					$tag_name_plus = '+' . $this_tag;
					$tag_name_plus_after = $this_tag . '+';
					$tag_name_equals = '=' . $this_tag;
					$tag_name_equals_plus = '=' . $this_tag . '+';

					if (strpos($url_after_tag, $tag_name_equals_plus) !== false) {

						// This tag is the first tag in URL and has others following it
						$url_update = str_replace($tag_name_plus_after,"",$url_after_tag);
						$url_update = $url_before_tag . $url_update;

					} else if (strpos($url_after_tag, $tag_name_plus) !== false) {

						// This tag is somewhere in the URL but not the beginning
						$url_update = str_replace($tag_name_plus,"",$url_after_tag);
						$url_update = $url_before_tag . $url_update;

					} else if (strpos($url_after_tag, $tag_name_equals) !== false) {

						// This tag is the first and only tag in URL
						$url_update = $url_before_tag;

					} else if (strpos($url_after_tag, $tag_name_plus) !== false) {

						// This tag is the first tag in URL and the last
						$url_update = $url_before_tag;

					} else {
						echo "Logic Error";
					}

					echo '<a class="btn tag current" href="'. $url_update . '" rel="tag">'.$tag->name.'</a> ';

				} else if (!in_array($tag->slug, $current_tags)) {

					echo '<a class="btn tag inactive" href="'.$url.'+' . $tag->slug .'" rel="tag">'.$tag->name.'</a> ';

				}
			}
		} else {
			foreach ((array) $tags as $tag) {
				$this_tag = ($tag->slug);
				echo '<a class="btn tag inactive" href="'.$url.'?tag=' . $this_tag .'" rel="tag">'.$tag->name.'</a> ';
			}
		}
		$tags = get_tags();
		$count = count( $tags );
		if ( $count > 0 ) {
			foreach ( $tags as $tag ) {
				if (!in_array($tag->term_id, $all_tag_ids)) { ?>
					<a class="btn tag disabled" href="#"><?php echo $tag->name;?></a>
				<?php }
			}
		}

		echo '</p>';
	}; ?>
</nav>
