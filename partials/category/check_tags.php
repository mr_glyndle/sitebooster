<?php $link_to_svg = get_template_directory_uri () . '/img/symbol-defs.svg';
$icon_count = 1;

if (check_tag('news') || check_tag('News')) { ?>
	<svg class="icon icon-news <?php echo 'icon_count_' . $icon_count; ?>"><use xlink:href="<?php echo $link_to_svg; ?>#icon-news"></use></svg>
<?php $icon_count++;
	};
if (check_tag('pdf') || check_tag('PDF')) { ?>
	<svg class="icon icon-pdf <?php echo 'icon_count_' . $icon_count; ?>"><use xlink:href="<?php echo $link_to_svg; ?>#icon-pdf"></use></svg>
<?php $icon_count++;
	};
if (check_tag('quote') || check_tag('Quote') || check_tag('testimonial') || check_tag('Testimonial')) { ?>
	<svg class="icon icon-quote <?php echo 'icon_count_' . $icon_count; ?>"><use xlink:href="<?php echo $link_to_svg; ?>#icon-quote"></use></svg>
<?php $icon_count++;
	};
if (check_tag('video') || check_tag('Video')) { ?>
	<svg class="icon icon-play <?php echo 'icon_count_' . $icon_count; ?>">"><use xlink:href="<?php echo $link_to_svg; ?>#icon-play"></use></svg>
<?php $icon_count++;
	};
