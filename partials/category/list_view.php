<?php while (have_posts()) : the_post(); ?>
	<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix scrollme'); ?>>
	  	<?php if (has_post_thumbnail()) { ?>
	    	<a class="thumbnail_link image_container inline-block vtop" href="<?php echo get_permalink(); ?>">
				<img class="lazyload" src="<?php
				$image = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'flex_height_small', false);
				echo esc_url($image[0]); ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>">
				<?php include locate_template('partials/category/check_tags.php'); ?>
	    	</a>
	  		<div class=" inline-block vtop part">
	    		<h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
				<?php $category_hide_date = get_field('category_hide_date'); if (isset($category_hide_date)) { } else { ?>
				<p class="the_date small"><?php the_modified_time(get_option('date_format')); ?></p>
				<?php }; ?>
			    <p class="small"><?php echo excerpt(33) ?> <a href="<?php echo get_permalink(); ?>">...continue reading</a></p>
				<?php the_tags( '<p class="small tags">', ' | ', '</p>' ); ?>
				<?php if (get_post_status() == 'private') { show_publish_button(); }; ?>
			</div>
		<?php } else { ?>
			<div class=" inline-block vtop full">
			    <h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
			    <p class="the_date small"><?php the_modified_time(get_option('date_format')); ?></p>
			    <p class="small"><?php echo excerpt(55) ?> <a href="<?php echo get_permalink(); ?>">...continue reading</a></p>
				<?php the_tags( '<p class="small tags">', ' | ', '</p>' ); ?>
				<?php if (get_post_status() == 'private') { show_publish_button(); }; ?>
			</div>
		<?php };?>
	</article>
<?php endwhile;
