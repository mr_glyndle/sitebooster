<article class="item ic_<?php echo $grid_item_count; ++$grid_item_count;?>">
	<a class="thumbnail_link image_container" href="<?php echo get_permalink(); ?>">
		<picture>
			<?php
			if (has_post_thumbnail()) {
				$post_thumb_id = get_post_thumbnail_id($id);
				$image_data = wp_get_attachment_image_src($post_thumb_id, 'square');
				$image_data_large = wp_get_attachment_image_src($post_thumb_id, 'square_medium');
				$image_data_small = wp_get_attachment_image_src($post_thumb_id, 'golden_small');
			} else {
				$image_data[0] = get_template_directory_uri() . '/img/placeholder_square.png';
				$image_data_large[0] = get_template_directory_uri() . '/img/placeholder_square.png';
				$image_data_small[0] = get_template_directory_uri() . '/img/placeholder_golden_small.png';
			}
			?>
			<!-- Show panoramic image and stack vertically when below 500px width -->
			<source 	media=			"(min-width: 500px)"
						data-srcset=	"<?php echo $image_data[0] ?>"
						sizes=			"33.3vw" />
			<!-- Show chosen image size above 500px width -->
			<source 	data-srcset=	"<?php echo $image_data_small[0]; ?>" />
			<img 		src=			"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw=="
						data-src=		"<?php echo $image_data[0] ?>"
						alt=			"<?php echo get_the_title(); ?>"
						class=			"lazyload"/>
		</picture>
	</a>


		<?php // include(locate_template('partials/category/check_tags.php')); ?>
		<a href="<?php echo get_permalink(); ?>" class="text">
			<div class="text_content">
				<h4><?php the_title(); ?></h4>
				<p class="small"><?php echo excerpt(28); ?></p>
				<?php if ( get_post_status () == 'private' ) {
					show_publish_button();
				};?>
			</div>
		</a>
</article>
