<?php while (have_posts()) : the_post(); ?>

  <article class="item ic_<?php echo $grid_item_count; ++$grid_item_count;?>">
	  <div class="txt_bg"></div>
		<a class="thumbnail_link image_container" href="<?php echo get_permalink(); ?>">
			<?php if (has_post_thumbnail()) {
				the_post_thumbnail('golden_medium', array('class' => 'lazyload'));
				include(locate_template('partials/category/check_tags.php'));
			} else {
				echo '<img class="placeholder" src="' . get_template_directory_uri() . '/img/placeholder.png" alt="' . get_the_title() . '">';
			}?>
		</a>
		<a class="text" href="<?php echo get_permalink(); ?>">
			<div class="text_content">

				<h4><?php the_title(); ?></h4>


				<?php $category_hide_date = get_field('category_hide_date', $post_id);
				if ($category_hide_date) {

				} else { ?>

					<p class="the_date small"><?php the_modified_time( get_option( 'date_format' ) ); ?></p>

				<?php }; ?>


				<p><?php echo excerpt(28); ?></p>


				<?php

					$i = 0;
					$posttags = get_the_tags();
					if (!empty($posttags)) {
						$len = count($posttags);
						echo '<p class="small tags">';
						foreach($posttags as $tag) {

							echo $tag->name;
							if ($i !== $len-1) {
						        echo ' | ';
						    }

							$i++;
						}
						echo '</p>';
					}
				?>


				<?php if ( get_post_status() == 'private' ) {

					show_publish_button();

				};?>

				<p class="small link"><span class="btn">More...</span></p>

			</div>
		</a>
  </article>

    <?php
    ++$i;
endwhile; ?>
