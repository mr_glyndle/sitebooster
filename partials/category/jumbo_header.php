<?php
$queried_object = get_queried_object();
if (isset($queried_object->taxonomy)) {
	$post_id = $queried_object->taxonomy.'_'.$queried_object->term_id;
	$background_image = get_field('category_image', $post_id);
	$txt_col = get_field('dark_or_light', $post_id);?>
<div class="cat_header <?php if (isset($txt_col)) { echo $txt_col; }; ?> parallax"><?php
if ($background_image) {

	$background_image = $background_image['sizes'];
	$bgimg_xl = $background_image['flex_height_xlarge'];
	$bgimg_l = $background_image['flex_height_large'];
	$bgimg_m = $background_image['flex_height_medium'];

	// Is there a webp version of the image and does the browser support webp?
	$webpver = $bgimg_m . '.webp';
	$webpver = strstr($webpver, 'wp-content/');

	if( is_file($webpver) ) {

			$bgimg_xlwebp = $bgimg_xl . '.webp';
			$bgimg_lwebp = $bgimg_l . '.webp';
			$bgimg_mwebp = $bgimg_m . '.webp';

	} else {

			$bgimg_xlwebp = $bgimg_xl;
			$bgimg_lwebp = $bgimg_l;
			$bgimg_mwebp = $bgimg_m;

	}
	$GLOBALS['footer-css'] .= "
	.cat_header {
		background-image: url('" . $bgimg_m ."');
	}
	.webp-support .cat_header {
		background-image: url('" . $bgimg_mwebp ."');
	}
	@media screen and (min-width: 989px) {
		.cat_header {
			background-image: url('" . $bgimg_m ."');
		}
		.webp-support .cat_header {
			background-image: url('" . $bgimg_mwebp ."');
		}
	}
	@media screen and (min-width: 1280px) {
		.cat_header {
			background-image: url('" . $bgimg_l . "');
		}
		.webp-support .cat_header {
			background-image: url('" . $bgimg_lwebp . "');
		}
	}
	@media screen and (min-width: 2000px) {
		.cat_header {
			background-image: url('" . $bgimg_xl . "');
		}
		.webp-support .cat_header {
			background-image: url('" . $bgimg_xlwebp . "');
		}
	}";
} ?>
	<div class="content txt_blk avs_<?php if ($background_image) { echo '10'; } else { echo 'default'; }; ?> normal " style="text-align: center;">
		<h1><?php single_cat_title(); ?></h1>
		<?php echo category_description(); ?>
	</div>
</div>
<?php }; ?>
