<?php
// Is it a category and if the text colour set?
if (is_category() || is_tax() || is_tag() || is_archive() ) {

	$queried_object = get_queried_object();
	if (isset($queried_object->taxonomy)) {
		$post_id = $queried_object->taxonomy.'_'.$queried_object->term_id;
		$txt_col = get_field('dark_or_light', $post_id);
	}

// Or, can we find the text colour of the header?
}

if(!isset($txt_col) && isset($head_set['txt_col'])) {

	$txt_col =  $head_set['txt_col'];

// We can't find the text colour set for the header
}

if(!isset($txt_col)) {

	$txt_col = 'dark';

}

// Is it dark?
if ($txt_col == 'dark') {

	// Use the dark logo
	$logo_svg = get_field('logo_svg', 'option');
	$logo_png = get_field('logo_png', 'option');

// Is it light?
} else {

	// Is the light logo set?
	// This here to offer support for older themes where there wasn't an option to set a light logo
	if ( get_field('logo_svg_light', 'option') || get_field('logo_png_light', 'option') ) {

		// Use the light logo
		$logo_svg = get_field('logo_svg_light', 'option');
		$logo_png = get_field('logo_png_light', 'option');

	} else {

		// Fallback to the dark (default) logo
		$logo_svg = get_field('logo_svg', 'option');
		$logo_png = get_field('logo_png', 'option');

	}
}


// Now we have the logo, what's it's URL?
if($logo_svg) { $logo_svg_url = $logo_svg['url']; }
if($logo_png) { $logo_png_url = $logo_png['url']; }

// Have we got an SVG and a PNG?
if ($logo_svg && $logo_png) { ?>

	<a class="branding logo" href="<?php $url = home_url( '/' ); echo esc_url( $url ); ?>">
		<img class="logo" src="<?php echo $logo_svg_url; ?>" onerror="this.onerror=null; this.src='<?php echo $logo_png_url; ?>'"  width="<?php echo $logo_png['width'] ?>" height="<?php echo $logo_png['height'] ?>" alt="<?php bloginfo( 'name' ); ?>" />
	</a>

<?php
// Have we only got a PNG?
} else if ($logo_png) { ?>

	<a class="branding logo" href="<?php $url = home_url( '/' ); echo esc_url( $url ); ?>">
		<img class="logo" src="<?php echo $logo_png_url; ?>" width="<?php echo $logo_png['width'] ?>" height="<?php echo $logo_png['height'] ?>" alt="<?php bloginfo( 'name' ); ?>" />
	</a>

<?php

// Have we only got an SVG?
} else if ($logo_svg) { ?>

	<a class="branding logo" href="<?php $url = home_url( '/' ); echo esc_url( $url ); ?>">
		<img class="logo" src="<?php echo $logo_svg_url; ?>" alt="<?php bloginfo( 'name' ); ?>" />
	</a>

<?php
// Can't find any logo set?
} else { ?>

	<a class="branding text" href="<?php $url = home_url( '/' ); echo esc_url( $url ); ?>">
		<?php bloginfo( 'name' ); ?>
	</a>

<?php };
