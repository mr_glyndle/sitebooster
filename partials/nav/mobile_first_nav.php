
<div class="get_around <?php if ($nav_text_colour) { echo $nav_text_colour; }; ?>">

	<div id="tbc-nav-icon" class="menu_toggle is-visible" data-nav-status="toggle">
		<span class="brgr"></span>
		<span class="brgr"></span>
		<span class="brgr"></span>
		<span class="brgr"></span>
		<?php if (class_exists('WooCommerce')) {

		  if (is_realy_woocommerce_page() || WC()->cart->get_cart_contents_count() !== 0) {

			  echo '<span class="cart-count">' . WC()->cart->get_cart_contents_count() . '</span>';

		  }
		}?>
		</div>

	<nav class="main-nav">

		<?php dynamic_sidebar('header'); ?>

		<?php wp_nav_menu(array('theme_location' => 'main_menu', 'depth' => 2, 'container' => false)); ?>

		<?php if (class_exists('WooCommerce')) {
			if (is_realy_woocommerce_page() || WC()->cart->get_cart_contents_count() !== 0) { ?>

				<div class="widget">
					<nav class="shop">
						<ul class="menu">
							<li class="menu-item menu-item-has-children">
								<a href="#shop">Cart &amp; Checkout</a>
								<ul class="sub-menu shop">
									<li><a href="/basket/">Basket</a></li>
									<li><a href="/checkout/">Checkout</a></li>
								</ul>
							</li>
						</ul>
					</nav>
					<?php if (is_user_logged_in()) { ?>
						<nav class="account">
							<ul class="menu">
								<li class=" menu-item-has-children">
									<a href="#account">Account</a>
									<ul class="sub-menu shop">
										<li><a href="/my-account/">Dashboard</a></li>
										<li><a href="/my-account/orders/">Orders</a></li>
										<li><a href="/my-account/downloads/">Downloads</a></li>
										<li><a href="/my-account/edit-address/">Addresses</a></li>
										<li><a href="/my-account/edit-account/">Account Details</a></li>
										<li><a href="/my-account/customer-logout/">Logout</a></li>
									</ul>
								</li>
							</ul>
						</nav>

					<?php }; ?>

				</div>

		<?php };
	}; ?>

	</nav>

</div>
