<?php $fixed_nav = get_field('fixed_nav', 'option');

if (is_page_template('no_chrome.php')) {
	$fixed_nav = 'mini';
}

// Is it a category and if the text colour set?
if (is_category() || is_tax() || is_tag() || is_archive() ) {

	$queried_object = get_queried_object();
	if (isset($queried_object->taxonomy)) {
		$post_id = $queried_object->taxonomy.'_'.$queried_object->term_id;
		$txt_col = get_field('dark_or_light', $post_id);
	}

// Or, can we find the text colour of the header?
}

if(!isset($txt_col) && isset($head_set['txt_col'])) {

	$txt_col =  $head_set['txt_col'];

// We can't find the text colour set for the header
}
?>

<div class="get_around <?php echo $fixed_nav; ?>">

	<div class="extras <?php if($txt_col) {echo $txt_col; } ?>">


		<?php  dynamic_sidebar('header');

		if (class_exists('WooCommerce')) {

			if (is_realy_woocommerce_page() || WC()->cart->get_cart_contents_count() !== 0) { ?>

				<nav class="shop inline-block vbaseline justify-item">
					<ul class="menu">
						<li class="menu-item menu-item-has-children">
							<a href="#shop"><svg class="icon icon-cart"><use xlink:href="<?php echo $link_to_svg; ?>#icon-cart"></use></svg><?php echo '<span class="cart-count">' . WC()->cart->get_cart_contents_count() . '</span>'; ?></a>
							<ul class="sub-menu">
								<li><a href="/basket/">Basket <?php echo '<span class="small">(' . WC()->cart->get_cart_contents_count() . ''; if( WC()->cart->get_cart_contents_count() > 1 ) { echo ' items)</span>'; } else { echo ' item)</span>'; }; ?></a></li>
								<li><a href="/checkout/">Checkout</a></li>
							</ul>
						</li>
					</ul>
				</nav>

			<?php if (is_user_logged_in()) { ?>

				<nav class="account inline-block vbaseline justify-item">
					<ul class="menu">
						<li class=" menu-item-has-children">
							<a href="#account"><svg class="icon icon-profile"><use xlink:href="<?php echo $link_to_svg; ?>#icon-profile"></use></svg></a>
							<ul class="sub-menu shop">
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Dashboard</a></li>
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>orders/">Orders</a></li>
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>downloads/">Downloads</a></li>
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>edit-address/">Addresses</a></li>
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>edit-account/">Account Details</a></li>
								<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>customer-logout/">Logout</a></li>
							</ul>
						</li>
					</ul>
				</nav>

			<?php };

			};

		} ?>


	</div>

	<div id="tbc-nav-icon" class="menu_toggle is-visible  <?php if($txt_col) {echo $txt_col; } ?>" data-nav-status="toggle">
	  <span class="brgr"></span>
	  <span class="brgr"></span>
	  <span class="brgr"></span>
	  <span class="brgr"></span>
	  <?php if (class_exists('WooCommerce')) {

		  if (is_realy_woocommerce_page() || WC()->cart->get_cart_contents_count() !== 0) {

			  echo '<span class="cart-count">' . WC()->cart->get_cart_contents_count() . '</span>';

		  }
	  }?>
	</div>

	<nav class="main-nav">

		<?php
			wp_nav_menu(array('theme_location' => 'main_menu', 'depth' => 2, 'container' => false));
		?>

		<div class="extras">


			<?php  dynamic_sidebar('header');

			if (class_exists('WooCommerce')) {

				if (is_realy_woocommerce_page() || WC()->cart->get_cart_contents_count() !== 0) { ?>

					<nav class="shop inline-block vbaseline justify-item">
						<ul class="menu">
							<li class="menu-item menu-item-has-children">
								<a href="#shop"><svg class="icon icon-cart"><use xlink:href="<?php echo $link_to_svg; ?>#icon-cart"></use></svg><?php echo '<span class="cart-count">' . WC()->cart->get_cart_contents_count() . '</span>'; ?></a>
								<ul class="sub-menu">
									<li><a href="/basket/">Basket <?php echo '<span class="small">(' . WC()->cart->get_cart_contents_count() . ''; if( WC()->cart->get_cart_contents_count() > 1 ) { echo ' items)</span>'; } else { echo ' item)</span>'; }; ?></a></li>
									<li><a href="/checkout/">Checkout</a></li>
								</ul>
							</li>
						</ul>
					</nav>

				<?php if (is_user_logged_in()) { ?>

					<nav class="account inline-block vbaseline justify-item">
						<ul class="menu">
							<li class=" menu-item-has-children">
								<a href="#account"><svg class="icon icon-profile"><use xlink:href="<?php echo $link_to_svg; ?>#icon-profile"></use></svg></a>
								<ul class="sub-menu shop">
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>">Dashboard</a></li>
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>orders/">Orders</a></li>
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>downloads/">Downloads</a></li>
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>edit-address/">Addresses</a></li>
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>edit-account/">Account Details</a></li>
									<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>customer-logout/">Logout</a></li>
								</ul>
							</li>
						</ul>
					</nav>

				<?php };

				};

			} ?>


		</div>

	</nav>

</div>
