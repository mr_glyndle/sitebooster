<?php
if (get_sub_field('sname')) {
	$slice_name = get_sub_field('sname');
	$txt_col =  get_sub_field('txt_col');
	$bg_cont = get_sub_field('bg_cont');
	if ($slice_name) {
		$slice_name = preg_replace("/[^A-Za-z0-9 ]/", '', $slice_name);
		$slice_ID = str_replace(' ', '', $slice_name);
	};
	if(isset($bg_cont)) {
		if ($bg_cont == "corg" && get_sub_field('bg_col')) {
			$bg_color = get_sub_field('bg_col');
			$bg_grad = get_sub_field('bg_grad');
			$bg_grad = get_sub_field('grad_dir');
		} else if ($bg_cont == "image" && get_sub_field('bg_img')) {
			$bg_img = get_sub_field('bg_img');
		} else if ($bg_cont == "video") {
			$bg_img = get_sub_field('vid_img');
			$background_video = true;
		} else {
			if (get_sub_field('bg_col')) {
				$bg_color = get_sub_field('bg_col');
				$bg_grad = get_sub_field('bg_grad');
				$bg_grad = get_sub_field('grad_dir');
				$bg_cont = 'corg';
			} else if (get_sub_field('bg_img')) {
				$bg_img = get_sub_field('bg_img');
				$bg_cont = 'image';
			}
		}
	}
	$bg_img_pos = get_sub_field('img_pos');
} else if (isset($slice_type) && $slice_type == 'menu_layout') {
	$slice_name = get_field('sname');
	$item_count = 0;
	$slice_type = 'top';
	$txt_col =  get_field('txt_col');
	$bg_cont = get_field('bg_cont');
	if ($slice_name) {
		$slice_name = preg_replace("/[^A-Za-z0-9 ]/", '', $slice_name);
		$slice_name = str_replace(' ', '_', $slice_name);
	};
	if(isset($bg_cont)) {
		if ($bg_cont == "corg" && get_field('bg_col')) {
			$bg_color = get_field('bg_col');
			$bg_grad = get_field('bg_grad');
			$w3_bg_grad_dir = get_field('grad_dir');
		}
		else if ($bg_cont == "image" && get_field('bg_img')) {
			$bg_img = get_field('bg_img');
		}
		else if ($bg_cont == "video") {
			$bg_img = get_sub_field('vid_img');
			$background_video = true;
		} else {
			if (get_field('bg_col')) {
				$bg_color = get_field('bg_col');
				$bg_grad = get_field('bg_grad');
				$w3_bg_grad_dir = get_field('grad_dir');
				$bg_cont = 'corg';
			} else if (get_field('bg_img')) {
				$bg_img = get_field('bg_img');
				$bg_cont = 'image';
			}
		}
	}
	$bg_img_pos = get_field('img_pos');
} else {
	$slice_name = 'header';
	$item_count = 0;
	$GLOBALS['item_count'] = 0;
	$slice_type = 'top';
	if(isset($head_set['txt_col'])) {
		$txt_col =  $head_set['txt_col'];
	}
	if(isset($head_set['bg_cont'])) {
		$bg_cont = $head_set['bg_cont'];
	}
	if(isset($bg_cont)) {
		if ($bg_cont == "corg") {
			$bg_color = $head_set['bg_col'];
			$bg_grad = $head_set['bg_grad'];
			$w3_bg_grad_dir = $head_set['grad_dir'];
		}
		if ($bg_cont == "image") {
			$bg_img = $head_set['bg_img'];
		}
		else if ($bg_cont == "video") {
			$bg_img = $head_set['vid_img'];
			$background_video = true;
		}
	} else {
		if (isset($head_set['bg_col'])) {
			$bg_color = $head_set['bg_col'];
			$bg_grad = $head_set['bg_grad'];
			$w3_bg_grad_dir = $head_set['grad_dir'];
			$bg_cont = 'corg';
		} else if (isset($head_set['bg_img'])) {
			$bg_img = $head_set['bg_img'];
			$bg_cont = 'image';
		}
	}
	if(isset($head_set['img_pos'])) {
		$bg_img_pos = $head_set['img_pos'];
	}
}

if (isset($bg_img)) {
	$bg_img = $bg_img['sizes'];
	$bgimg_xxl = $bg_img['flex_height_xxlarge'];
	$bgimg_xl = $bg_img['flex_height_xlarge'];
	$bgimg_l = $bg_img['flex_height_large'];
	$bgimg_m = $bg_img['flex_height_medium'];
}
if (isset($bg_grad)) {
	if ($bg_grad == 'top') {
		$w3_bg_grad_dir = 'bottom';
	} else if ($bg_grad == 'left') {
		$w3_bg_grad_dir = 'right';
	}
}
?>

<div <?php if (isset($slice_ID)) { echo 'id="' . $slice_ID . '"'; } else { echo 'id="slice_' . $item_count . '"'; };?> class="slice <?php echo $slice_type . ' ';?> slice_<?php echo $item_count;  echo ' ' . $txt_col . ' ' . $slice_name;
	if ( (isset($bg_img_pos) && $bg_img_pos !== "scroll") || !isset($bg_img_pos) ) {
		if ( isset($bg_cont) && ($bg_cont == "image" && ($bg_img_pos !== "scroll" || !isset($bg_img_pos) ))) {
			echo ' parallax';
		} if ( $bg_cont == "video" ) {
			echo ' video parallax';
		}
	}
	if ( ( isset($bg_color) && isset($bg_grad) ) && ( $bg_img_pos !== "scroll" || !isset($bg_img_pos ) ) ) {
		echo ' parallax';
	};?> ">
	<?php
	if ((isset($bg_cont)) && ($bg_cont == "corg")) {
		if (isset($bg_color) || isset($bg_grad)) {
			$GLOBALS['footer-css'] .= " .slice_" . $item_count . " {";
			if ($bg_color) {
				$GLOBALS['footer-css'] .= "background:" . $bg_color . ";";
			};
			if ($bg_grad) {
				$GLOBALS['footer-css'] .= "background: linear-gradient(to " . $w3_bg_grad_dir . ", " . $bg_grad . " 0%," . $bg_color . " 100%);";
			};
			if ( $bg_cont == "corg" && $bg_img_pos !== "scroll") {
				$GLOBALS['footer-css'] .= "background-attachment: fixed;";
			}
			$GLOBALS['footer-css'] .= "}";
		}
	}
	if ((isset($bg_cont)) && ($bg_cont == "image" || $bg_cont == "video") && (isset($bg_img))) {

		// Is there a webp version of the image and does the browser support webp?
		$webpver = $bgimg_m . '.webp';
		$webpver = strstr($webpver, 'wp-content/');

		if( is_file($webpver) ) {

				$bgimg_xxlwebp = $bgimg_xxl . '.webp';
				$bgimg_xlwebp = $bgimg_xl . '.webp';
				$bgimg_lwebp = $bgimg_l . '.webp';
				$bgimg_mwebp = $bgimg_m . '.webp';

		} else {

				$bgimg_xxlwebp = $bgimg_xxl;
				$bgimg_xlwebp = $bgimg_xl;
				$bgimg_lwebp = $bgimg_l;
				$bgimg_mwebp = $bgimg_m;

		}

		$GLOBALS['footer-css'] .= ".slice.slice_" . $GLOBALS['item_count'] . " {
			background-image: url('" . $bgimg_m ."');
		}
		.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
			background-image: url('" . $bgimg_mwebp ."');
		}
		@media screen and (max-width: 700px), screen and (min-height: 700px) {
			.slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_m ."');
			}
			.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_mwebp ."');
			}
		}
		@media screen and (min-width: 1000px), screen and (min-height: 700px) {
			.slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_l . "');
			}
			.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_lwebp . "');
			}
		}
		@media screen and (min-width: 1800px), screen and (min-height: 1000px) {
			.slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_xl . "');
			}
			.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_xlwebp . "');
			}
		}
		@media screen and (min-width: 2500px), screen and (min-height: 1800px) {
			.slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_xxl . "');
			}
			.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_xxlwebp . "');
			}
		}";
	}
	if ((isset($bg_cont)) && ($bg_cont == "image" || $bg_cont == "corg")) {
		if ((isset($bg_color)) || (isset($bg_img))) { ?>
		<?php }
	}
	if (isset($background_video)) {
		include(locate_template('partials/video_bg.php'));
	}

	// clear variables
	unset($bg_color);
	unset($bg_grad);
	unset($bg_grad);
	unset($w3_bg_grad_dir);
	unset($bg_img);
	unset($txt_col);
?>
