<?php
$use_header_image = get_field('head_bg');
if( $use_header_image == "yes" || $use_header_image == "custom") {

		function top_header_image() {

			$header_height = get_field('header_height');
			$attachment_id = get_field('header_image');
			$header_content = get_field('header_content');
			$custom_header_content = get_field('custom_header_content');

			$add_vertical_space = get_sub_field('add_space');

			$rgba_colour = get_field('over_color');
			if (get_field('text_overlay_opacity')) {
				$a = get_field('text_overlay_opacity');
				$Hex_color = str_replace("#", "", $rgba_colour);
				if(strlen($Hex_color) == 3) {
					$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
					$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
					$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
				} else {
					$r = hexdec(substr($Hex_color,0,2));
					$g = hexdec(substr($Hex_color,2,2));
					$b = hexdec(substr($Hex_color,4,2));
				}
				$rgb = array($r, $g, $b);
				$Rgb_color = implode(", ", $rgb);
				$rgba_colour = 'rgba(' . $Rgb_color . ',' . $a . ')';
			};

			if (!$attachment_id) {
				if (has_post_thumbnail()) {
					$current_id = get_the_ID();
					$attachment_id = get_post_thumbnail_id($current_id);
				}
			}
			if ($attachment_id) {

				$alt_text = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
				if ( !$alt_text ) { $alt_text = esc_html( get_the_title() ); }

				$thumb_xxl 		= 	wp_get_attachment_image_src($attachment_id, 'flex_height_xxlarge');
				$thumb_xl 		= 	wp_get_attachment_image_src($attachment_id, 'flex_height_xlarge');
				$thumb_large    = 	wp_get_attachment_image_src($attachment_id, 'flex_height_large');
				$thumb_medium   = 	wp_get_attachment_image_src($attachment_id, 'flex_height_medium');
				$thumb_small    = 	wp_get_attachment_image_src($attachment_id, 'flex_height_small');
				?>

					<div class="bgimg">

						<img srcset="<?php echo $thumb_xl[0]; ?>  3000w,
						<?php echo $thumb_large[0]; ?>  2000w,
						<?php echo $thumb_medium[0]; ?>   1280w,
						<?php echo $thumb_medium[0]; ?>   898w"
						sizes="100vw"
						src="<?php echo $thumb_small[0]; ?>"
						class="lazyload"
						alt="<?php echo $alt_text?>">

					</div>
					<?php $add_vertical_space = get_field('add_space');
					if ($header_content !== 'none') {?>
					<div <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?> class="content txt_blk normal <?php if ($add_vertical_space) { echo 'avs_' . $add_vertical_space; }; ?>">
						<div class="text_content"><?php
							if ($header_content == "title") {
								echo "<h1 style='text-align: center;'>" . esc_html( get_the_title() ) . "</h1>";
							} else if ($header_content == "custom") {
								echo $custom_header_content;
							}?>
						</div>
					</div>
				<?php } else {

				};
			}
		};?>
<?php
	if ( is_singular() ) {
		if (class_exists( 'woocommerce' )) {
			if (!is_woocommerce()) {
				top_header_image();
			}
		} else {
			top_header_image();
		}
	}
}
?>
