<?php
background('gallery p-gall');
$alignment_over_background = '';
overlay($alignment_over_background);

$images = get_sub_field('gallery');
$number_in_grid = count($images);
$full_width = get_sub_field('full_width');
if( is_array($full_width) ) {
	$full_width = $full_width[0];
} else {
	unset($full_width);
}

$lightbox = get_sub_field('open_images_in_lightbox');
$carousel = get_sub_field('make_carousel');
$global_link_for_images = get_sub_field('global_link_for_images');
$grid_item_count = 1;
if( is_array($carousel) ) {
	if(isset($carousel[0])) {
		$carousel = $carousel[0];
	}
} else {
	unset($carousel);
}
if( is_array($lightbox) ) {
	$lightbox = $lightbox[0];
} else {
	unset($lightbox);
}

if( $images ): ?>
	<?php if( isset($carousel) && $carousel == "Yes" ) {
	    echo '<div class="carousel items owl-carousel owl-theme ">';
	} else {
		echo '<div class="contained">';
	}; ?>
	<?php foreach( $images as $image ):
		$custom_link = get_field('custom_link', $image['ID']); ?>
		<div class="thumb ic_<?php echo $grid_item_count; ++$grid_item_count;?>">

			<?php if ($lightbox == 'Yes' || ($global_link_for_images) || ($custom_link) ) { ?>
				<a class="thumb_link <?php if ($global_link_for_images) { echo '" href="' . $global_link_for_images . '"'; } else if ($custom_link) { echo '" href="' . $custom_link . '"'; } else { if ($lightbox == 'Yes') { echo 'lightmeup lightbox' . $item_count . '" href="' . $image['sizes']['flex_height_xlarge'] . '"'; } }; ?>>
			<?php } ?>

					<img alt="<?php echo $image['alt']; ?>" title="<?php echo $image['title']; ?>"
					<?php if( isset($carousel) && $carousel !== "Yes" ) {
						echo 'src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" ';
						echo 'data-src="' . $image['sizes']['flex_height_small'] . '" ';
						echo 'width="' . $image['sizes']['flex_height_small-width'] . '" ';
						echo 'height="' . $image['sizes']['flex_height_small-height'] . '" ';
						echo 'class="lazyload"';
					} else {
						echo 'src="' . $image['sizes']['flex_height_small'] . '" ';
						echo 'width="' . $image['sizes']['flex_height_small-width'] . '" ';
						echo 'height="' . $image['sizes']['flex_height_small-height'] . '" ';
					}; ?>/>

			<?php if ($lightbox == 'Yes' || ($global_link_for_images) || ($custom_link) ) { ?>
				</a>
			<?php } ?>

			<?php if ($image['caption']) {?>
				<div class="text">
					<p><?php echo $image['caption'];?></p>
				</div>
			<?php }; ?>

		</div>
	<?php endforeach;

echo '</div></div>';

endif;


if( isset($carousel) && $carousel == "Yes" ) {
	include locate_template('partials/carousel_script.php');
};
unset($carousel);?>
<?php if ($lightbox == 'Yes') {
	include locate_template('partials/lightbox_script.php');
};?>
</div>
