<?php

if ($slice_name == 'header') {
	if(isset($head_set['vid_img'])) {
		$video_img = $head_set['vid_img'];
	}
} else {
	$video_img = get_sub_field('vid_img');
}
if (isset($bg_cont) && $bg_cont == "video") {

	if ($slice_name == 'header') {
		$yt_id = $head_set['yt_id'];
	} else {
		$yt_id = get_sub_field('yt_id');
	}

	if ($yt_id) {
		$yt_div_id = preg_replace('/[^a-z]/i','', $yt_id);
	} else {
		if ($slice_name == 'header') {
			$video_mp4 = $head_set['mp4'];
			$video_ogg = $head_set['ogg'];
			$video_webm = $head_set['webm'];
		} else {
			$video_mp4 = get_sub_field('mp4');
			$video_ogg = get_sub_field('ogg');
			$video_webm = get_sub_field('webm');
		}
	};

	if ($slice_name == 'header') {
		$loop_video = $head_set['loop'];
		$mute_video = $head_set['mute'];
	} else {
		$loop_video = get_sub_field('loop');
		$mute_video = get_sub_field('mute');;
	}?>
	<div class="video_bg <?php if ($yt_id) { echo ' youtube'; } else { echo ' native'; }; ?>">
		<?php if ($yt_id) { ?>
			<div class="youtube_container">
				<div id="yt_<?php echo $yt_div_id; ?>"></div>
			</div>

		<?php } else { ?>

		<video class="fullscreen" <?php if ($mute_video) { ?>muted<?php }; ?> autoplay="true" preload="auto" <?php if ($loop_video[0] == "loop") { echo 'loop="true"'; }; ?>>


				<?php if ($video_mp4) { ?>
						<source src="<?php echo $video_mp4; ?>" type="video/mp4" /><?php if ($video_img) { echo '<img src="'.$video_img.'">'; };
					}; ?>
				<?php if ($video_ogg) { ?>
						<source src="<?php echo $video_ogg; ?>" type="video/ogg" /><?php if ($video_img) { echo '<img src="'.$video_img.'">'; };
					}; ?>
				<?php if ($video_webm) { ?>
						<source src="<?php echo $video_webm; ?>" type="video/webm" /><?php if ($video_img) { echo '<img src="'.$video_img.'">'; };
					}; ?>

		</video>

		<?php };
		$link_to_svg = get_template_directory_uri () . '/img/symbol-defs.svg'; ?>

	</div>

	<div id="bgvc" class="video controls">
	  <svg class="button icon ytplay" id="playbutton_<?php echo $yt_div_id; ?>">
	    <use xlink:href="<?php echo $link_to_svg; ?>#icon-play"></use>
	  </svg>
	  <svg class="button icon ytpause" id="pausebutton_<?php echo $yt_div_id; ?>">
	    <use xlink:href="<?php echo $link_to_svg; ?>#icon-pause"></use>
	  </svg>
	</div>
	<?php if (!isset($GLOBALS['youtubeAPI'])) {
		$GLOBALS['youtubeAPI'] = "
			var tag = document.createElement('script');
			tag.src = 'https://www.youtube.com/iframe_api';
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
			var player;
			function onYouTubeIframeAPIReady() {";
	};

	$GLOBALS['youtubeAPI'] .= "
	var isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
	if (isMobile) {
		document.getElementById('bgvc').style.visibility = 'hidden';
	} else {
		yt_" . $yt_div_id . " = new YT.Player('yt_" . $yt_div_id . "', {
			height: '100%',
			width: '100%',
			playerVars: {
				autoplay: 1,
				loop: 1,
		    	playlist: '" . $yt_id . "',
				controls: 0,
				showinfo: 0,
				autohide: 1,
				modestbranding: 1,
				iv_load_policy: 3,
				rel: 0,
				" . (($mute_video[0]=='mute')?'mute:1':"") . "
			},
			videoId: '" . $yt_id . "'
	    });

		var element = document.getElementById('playbutton_" . $yt_div_id . "');
		element.classList.add('active');
		var element = document.getElementById('pausebutton_" . $yt_div_id . "');
		element.classList.remove('active');

		jQuery('#pausebutton_" . $yt_div_id . "').click( function() {
		   yt_" . $yt_div_id . ".pauseVideo();
		   var element = document.getElementById('playbutton_" . $yt_div_id . "');
		   element.classList.remove('active');
		   var element = document.getElementById('pausebutton_" . $yt_div_id . "');
		   element.classList.add('active');
		});
		jQuery('#playbutton_" . $yt_div_id . "').click( function() {
		   " . "yt_" . $yt_div_id . ".playVideo();
		   var element = document.getElementById('playbutton_" . $yt_div_id . "');
		   element.classList.add('active');
		   var element = document.getElementById('pausebutton_" . $yt_div_id . "');
		   element.classList.remove('active');
		});
	}"; ?>

<?php } ?>
