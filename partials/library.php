<?php

$get_slices_from = get_sub_field('library');

if (isset($get_slices_from) && $get_slices_from !== '') {

	foreach ($get_slices_from as $libraryID) {

		if( have_rows('cont', $libraryID) ) {

			// loop through the rows of data
			while ( have_rows('cont', $libraryID) ) : the_row();

				if(get_post_status($libraryID) == 'publish') {

					include(locate_template('partials/slice_loop.php'));

				}

			endwhile;

		} else {

		}
	}
}
// unset($last_libraryID);
unset($get_slices_from);
