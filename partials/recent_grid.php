<?php
$show_intro = get_sub_field('text_ab');
$show_outro = get_sub_field('text_be');
$date = get_sub_field('date');
$intro = get_sub_field('intro');
$outro = get_sub_field('outro');
$number_in_grid = get_sub_field('number_of_posts_to_show');
$order_by = get_sub_field('order_by');
$order = get_sub_field('order');

include(locate_template('partials/grid_items/get_recent_posts.php'));

//start of the loop
if ($get_posts_from_type !== 'event') {
	$myposts = get_posts( $args );
	$post_is_event = false;
}

if(count($myposts) > 0) {
	$number_in_grid = count($myposts);
	include(locate_template('partials/grid_items/grid_variables.php'));
	background('grid_cont');
	include(locate_template('partials/overlay.php'));
	include(locate_template('partials/spacing.php'));
?>
	<div class="s_over <?php echo $container_class ?> <?php if ($text_align) { echo $text_align; }; ?>">
		<div class="contain avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; };?>">
			<?php if(($intro && !isset($show_intro)) || ($intro && $show_intro == "yes")) { echo '<div class="intro txt_blk">' . $intro . '</div>'; };?>
		    <div class="items <?php if ($number_in_grid){ echo 'ti_' . $number_in_grid . ' '; }; if ($carousel == 'yes') { echo 'owl-carousel owl-theme '; }; ?>" >

				<?php
				foreach ( $myposts as $post ) : setup_postdata( $post );
					include(locate_template('partials/grid_items/post_content.php')); ?>
					<div class=" item ic_<?php echo $grid_item_count;?>" >
						<div class="txt_bg" <?php if ($rgba_colour && strpos($container_class, 'no_overlay') !== false) { echo 'style="background-color:' . $rgba_colour . '"';};?>></div>
						<?php include(locate_template('partials/grid_items/grid_images.php'));?>

						<?php if ($text_align == 'center' && $overlay) { ?>
							<a class="text" href="<?php echo $profile_grid_link_to; ?>" <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?>>
								<div class="text_content">
				        	<?php echo '<h4>' . $profile_grid_title . '</h4>';
									if ($profile_grid_main && $profile_grid_main !== '') {
										echo $profile_grid_main;
									} ?>
			  					</div>
								<?php if ($post_is_event) {
									$event_date = tribe_get_start_date( null, false, Tribe__Date_Utils::DBDATEFORMAT );
									$event_date = explode('-', $event_date);
									$dateObj   = DateTime::createFromFormat('!m', $event_date[1]);
									$monthName = $dateObj->format('M'); // March
									echo '<div class="date"><span class="day">' . $event_date[2] . '</span><span class="month">' . $monthName . '</span></div>';
								} ?>
							</a>
						<?php } else {

							?>
							<a class="text" href="<?php echo $profile_grid_link_to; ?>">
								<div class="text_content">
									<?php echo '<h4>' . $profile_grid_title;
									if( function_exists('tribe_get_cost')) {
										if ( tribe_get_cost() ) {
											echo '<span class="small price"> - ' . tribe_get_cost( null, true ) . '</span>';
										}
									};
									echo '</h4>';

									if ($date && $date[0] == '1') {
										echo '<p class="small mod-date">' . $profile_grid_date . '</p>';
									}
									if ($profile_grid_main !== '') {
										echo $profile_grid_main;
									}; ?>
									<p class="small link"><span class="btn">More...</span></p>


									<?php if (strpos($container_class, 'no_overlay') !== false) {
										// If it's accommodation, get the custom field data to show below the text
										if ($get_posts_from_type == 'accom' || $get_posts_from_type == 'accom_f') {

												$max_occupancy = get_field('max_occupancy');
												$no_beds = get_field('no_beds');
												$no_rooms = get_field('no_rooms');
												$pets = get_field('pets');

												echo '<p class="small tags">';
												if($no_rooms) {
													echo '<span><svg class="icon icon-single_bed"><use xlink:href="'. $link_to_svg .'#icon-single_bed"></use></svg><b>'.$no_rooms . '</b> bedroom';
													if ($no_rooms > 1) {
														echo 's';
													}
													echo '</span>';
												}
												if ($no_beds) {
													echo '<span><svg class="icon icon-single_bed"><use xlink:href="'. $link_to_svg .'#icon-single_bed"></use></svg><b>'.$no_beds.'</b> bed';
													if ($no_beds > 1) {
														echo 's';
													}
													echo '</span>';
												}
												if($max_occupancy) {
													echo '<span><svg class="icon icon-person_outline"><use xlink:href="'. $link_to_svg .'#icon-person_outline"></use></svg>Up to <b>' . $max_occupancy . '</b> guests</span>';
												}
												if ($pets == "True") {
													echo '<span><svg class="icon icon-pets"><use xlink:href="'. $link_to_svg .'#icon-pets"></use></svg>Pet Friendly</span>';
												}
												echo '</p>';


												echo '<p class="small tags"><svg class="icon icon-location"><use xlink:href="'. $link_to_svg .'#icon-location"></use></svg>';
												$categories = get_the_terms( $post->ID, 'accommodation_type' );
												$i = 1;
												$c = count($categories);
												foreach( $categories as $category ) {
													echo $category->name;
													if (($i < $c) && ($c !== 1)) {
														echo ', ';
													}
													++$i;
												}
												echo '</p>';
											}
										}	?>
								</div>
							</a>
						<?php if ($post_is_event) {
							$event_date = tribe_get_start_date( null, false, Tribe__Date_Utils::DBDATEFORMAT );
							$event_date = explode('-', $event_date);
							$dateObj   = DateTime::createFromFormat('!m', $event_date[1]);
							$monthName = $dateObj->format('M'); // March
							echo '<div class="date"><span class="big day">' . $event_date[2] . '</span><span class="small month">' . $monthName . '</span></div>';
						};
					};

					if (strpos($container_class, 'no_overlay') !== false) {

						// Grid item is displaying text below image

						if ($rgba_colour !== '') {

							// Item uses slice wide overlay
							$GLOBALS['footer-css'] .= '
							.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ' .txt_bg { background-color: ' . $rgba_colour . '; transition: background-color ease .3s;}
							.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .ic_' . $grid_item_count . ':hover .txt_bg { background-color: rgba(' . $Rgb_color . ',.9);}';

						}

					} else {

						// Grid item is overlaying text over image

						if ($text_align == 'center' && $overlay) {

							if (isset($rgba_colour) && $rgba_colour !== '') {

								$GLOBALS['footer-css'] .= '
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background-color: ' . $rgba_colour . '; }
								.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background-color: rgba(' . $Rgb_color . ', .2); }';

							}

						} else {

							// Grid item is left or right aligned, so we need a gradient overlay

							if ($rgba_colour !== '') {

								$GLOBALS['footer-css'] .= '
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ' a.text { background: linear-gradient(to bottom, rgba(' . $Rgb_color . ',0) 0%, ' . $rgba_colour . ' 100%); }
									.override .slice_' . $GLOBALS['item_count'] . '.grid_cont .grid.overlay .ic_' . $grid_item_count . ':hover a.text { background: linear-gradient(to bottom, rgba(' . $Rgb_color . ',0) 0%, rgba(' . $Rgb_color . ', 1) 30%); }';

							}

						}

					} ?>
				</div>
				<?php ++$grid_item_count;
			    wp_reset_postdata();
			    endforeach;
				echo '</div>';
				if(($outro && !isset($show_outro)) || ($outro && $show_outro == "yes")) { echo '<div class="outro txt_blk">' . $outro . '</div>'; };?>
			</div>
		</div>
	</div>
	<?php if ($carousel == 'yes') {
	    include locate_template('partials/grid_items/carousel_script.php');
		$bg_color = get_sub_field('bg_col');
		$slice_name = get_sub_field('sname');
		if ($slice_name) {
			$slice_name = preg_replace("/[^A-Za-z ]/", '', $slice_name);
			$slice_ID = str_replace(' ', '', $slice_name);
		};
		if ($bg_color) {
			$Hex_color = str_replace("#", "", $bg_color);
			if(strlen($Hex_color) == 3) {
				$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
				$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
				$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
			} else {
				$r = hexdec(substr($Hex_color,0,2));
				$g = hexdec(substr($Hex_color,2,2));
				$b = hexdec(substr($Hex_color,4,2));
			}
			$rgb = array($r, $g, $b);
			$Rgb_color = implode(", ", $rgb);
		};
		$rgba_from_colour = 'rgba(' . $Rgb_color . ',1)';
		$rgba_to_colour = 'rgba(' . $Rgb_color . ',0)';

		$GLOBALS['footer-css'] .= '#' . $slice_ID . ' .owl-theme .owl-nav .owl-next { background: linear-gradient(to right, ' . $rgba_to_colour . ' 0%, ' . $rgba_from_colour . ' 100%)} #' . $slice_ID . ' .owl-theme .owl-nav .owl-prev { background: linear-gradient(to right, ' . $rgba_from_colour . ' 0%, ' . $rgba_to_colour . ' 100%)}';
	}
} ?>
