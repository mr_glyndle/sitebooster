<?php
/*
Template Name: Standard Wordpress page
Description: No fancy page builder
*/
?>

 <?php get_header(); ?>
	<?php if (have_posts()) : ?>
		<article>
			<div class="container main">
			<?php while (have_posts()) : the_post(); ?>

				<div class="row">
				  <div class="txt_blk">
						<?php the_content();?>
					</div>
				</div>

				<?php 	previous_post_link( '%link', 'Previous post in this category', TRUE, ' ', 'post_format' );
					next_post_link( '%link', 'Next post in this category', TRUE, ' ', 'post_format' ); ?>

					<?php
            if (is_singular()) {
              if (comments_open() || get_comments_number()) { ?>
								<div class="row">
                	<?php comments_template(); ?>
								</div>
            <?php };
            }; ?>

	   	<?php endwhile; ?>

			<?php else : ?>


				<div class="txt_blk">
					<div class="alert alert-info">
						<h1>Sorry, we can't find the page you're looking for</h1>
						<p>Please use the navigation or, seach the site with the options in the menu.</p>
					</div>
				</div>


			<?php endif; ?>
		</div>
</article>

<?php get_footer(); ?>
