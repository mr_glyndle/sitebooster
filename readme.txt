<h1>Brand Chap core theme</h1>
<p>Starting point for Brand Chap wordpress projects</p>
<h3 id="header-file-includes">Header file includes</h3>
<ul>
<li>Jquery js from google api</li>
<li>Menu</li>
</ul>
<h3 id="index-file-includes">Index file includes</h3>
<ul>
<li>Custom loop of content building blocks</li>
<li>Sidebar</li>
</ul>
<h3 id="function-file-include">Function file include</h3>
<ul>
<li>Post thumbnails</li>
<li>Menu Support</li>
<li>Registered sidebar</li>
</ul>
