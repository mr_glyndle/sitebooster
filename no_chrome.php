<?php
/*
Template Name: No Chrome
Description: No interface (no header, no footer), just content and mini nav
*/
?>

 <?php get_header();
 if (have_posts()) {
 	while (have_posts()) : the_post();

 		// check if the flexible content field has rows of data
 	    if( have_rows('cont') ) {

				$item_count = 1;
				$GLOBALS['item_count'] = "1";

 				// loop through the rows of data
 				while ( have_rows('cont') ) : the_row();

					include(locate_template('partials/slice_loop.php'));

 				endwhile;

 			} else { ?>
 				<div class="txt_blk"><?php the_content();?></div>
 		 	<?php }; ?>

	<?php endwhile; ?>
<?php } else { ?>

	<div class="txt_blk">
		<div class="alert alert-info">
			<h1>Sorry, we can't find the page you're looking for</h1>
			<p>Please use the navigation or, seach the site with the options in the menu.</p>
		</div>
	</div>

<?php }; ?>

 <?php get_footer(); ?>
