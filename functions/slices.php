<?php
function file_get_contents_ssl($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_REFERER, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3000); // 3 sec.
    curl_setopt($ch, CURLOPT_TIMEOUT, 10000); // 10 sec.
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function standardslice() {

	background('text');
	$alignment_over_background = get_sub_field('bg_align');
	overlay($alignment_over_background);

	if (isset($alignment_over_background) && $alignment_over_background == 'normal') {
		echo '<div class="text_content">';
	};

	the_sub_field('txt_cont');

	if (isset($alignment_over_background) && $alignment_over_background == 'normal') {
		echo '</div>';
	};?>

	</div>
</div>
<?php }

function textandimage() {
background('text_image');
$alignment_over_background = '';
overlay($alignment_over_background);

$alignemnt = get_sub_field('alignment');
$distribution = get_sub_field('distribution');
$remove_image_space = get_sub_field('remove_image_space');

$vertical_image_alignment = get_sub_field_object('align_image');
$mobile_stack = get_sub_field('mob_stac');
$vertical_image_alignment = $vertical_image_alignment['value'];
if( $vertical_image_alignment == 'vtop' ) {
	$vertical_image_alignment = 'vtop';
} else if( $vertical_image_alignment == 'vbottom' ) {
	$vertical_image_alignment = 'vbottom';
} else  {
	$vertical_image_alignment = 'vmiddle';
};

$image_space_to_remove = '';
if( $remove_image_space ):
	foreach( $remove_image_space as $v ):
		$image_space_to_remove = $image_space_to_remove . 'nopad_' . $v . ' ';
	endforeach;
endif;
$image_and_text_image = get_sub_field('image_left_image');
$image_and_text_header = get_sub_field('image_left_header');
$image_and_text_text = get_sub_field('image_left_text');
$image_and_text_link_url = get_sub_field('link_button_url');
$image_and_text_link_text = get_sub_field('link_button_text');
$animate = ' ';
$animate = get_sub_field('animate');
if ($animate) {
	$animate = $animate[0];
};?>

	<div class="content t_and_i image_<?php echo $alignemnt . ' ' . $distribution; ?> item_<?php echo $GLOBALS['item_count']; if ($animate) { echo ' scrollme';}; if (!empty($mobile_stack)) { if( $mobile_stack[0] == 'img_t' ) { echo ' image_top'; } else if ($mobile_stack == 'img_b') { echo ' image_bottom'; }}; ?>">

		<?php if ($alignemnt == 'left') { ?>

			<div class="image inline-block <?php if ($image_space_to_remove) { echo $image_space_to_remove; }; echo $vertical_image_alignment; ?> ">

				<?php if( strpos( $image_and_text_image['sizes']['flex_height_medium'], '.svg' ) !== false) { ?>

					<div class="<?php if($animate) { echo 'animateme scrollme'; } else { echo 'lazyload'; }; ?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?>">

						<?php $svg_content = file_get_contents_ssl( $image_and_text_image['sizes']['flex_height_small'] );
						echo preg_replace('/width="[\s\S]+?"/', '', $svg_content); ?>

					</div>

			<?php } else { ?>

			<img class="<?php if ($animate) { echo 'animateme scrollme'; } else {echo 'lazyload'; }; ?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?>"
				<?php if ($animate) { include(locate_template('partials/animate.php')); };

					echo 'srcset="'. $image_and_text_image['sizes']['flex_height_medium'] .' 898w,'
					. $image_and_text_image['sizes']['flex_height_small'] .' 500w"
					sizes="	(min-width: 992px) 45vw, 95vw"
					src="'. $image_and_text_image['sizes']['flex_height_small'].' "
					alt=		"'. $image_and_text_image['alt'] .'"
					title=		"'. $image_and_text_image['title'].'" />'; ?>
				<?php }; ?>

		</div>

		<?php }; ?>

	  <div 	class="text inline-block vmiddle">

		<?php echo $image_and_text_text; ?>

	  </div>

		<?php if ($alignemnt == 'right') { ?>

			<div class="image inline-block <?php if ($image_space_to_remove) { echo $image_space_to_remove; }; echo $vertical_image_alignment; ?> ">

				<?php if( strpos( $image_and_text_image['sizes']['flex_height_medium'], '.svg' ) !== false) { ?>

					<div class="<?php if($animate) { echo 'animateme scrollme'; } else { echo 'lazyload'; }; ?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?>">

						<?php
							$svg_content = file_get_contents_ssl( $image_and_text_image['sizes']['flex_height_small'] );
							echo preg_replace('/width="[\s\S]+?"/', '', $svg_content);
						?>

					</div>

			<?php } else { ?>

			<img class="<?php if ($animate) { echo 'animateme scrollme'; } else {echo 'lazyload'; }; ?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?>"
				<?php if ($animate) { include(locate_template('partials/animate.php')); };

					echo 'srcset="'. $image_and_text_image['sizes']['flex_height_medium'] .' 898w,'
					. $image_and_text_image['sizes']['flex_height_small'] .' 500w"
					sizes="	(min-width: 992px) 45vw, 95vw"
					src="'. $image_and_text_image['sizes']['flex_height_small'].' "
					alt=		"'. $image_and_text_image['alt'] .'"
					title=		"'. $image_and_text_image['title'].'" />'; ?>
				<?php }; ?>

		</div>

		<?php }; ?>

	</div>
</div>
</div>
<?php }

function twocolumn() {

	background('two_column');
	$alignment_over_background = '';
	overlay($alignment_over_background);

	$intro_text = get_sub_field('intro_text');
	$distribution = get_sub_field('distribution');
	$align = get_sub_field('align');
?>
		<div class="two_column <?php echo $distribution; ?>">

			<?php if ($intro_text) {
				echo '<div class="intro">' . $intro_text . '</div>';
			}; ?>

			<div class="column left <?php echo 'v' . $align; ?>">
			    <?php the_sub_field('left_column'); ?>
			</div>
			<div class="column right <?php echo 'v' . $align; ?>">
			    <?php the_sub_field('right_column'); ?>
			</div>
		</div>
	</div>
</div>

<?php }
