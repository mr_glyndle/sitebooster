<?php //function to print publish button
function show_publish_button() {
	Global $post;
	//only print if admin
	if (current_user_can('edit_others_posts')) {
		echo '<p><form action="" method="POST" name="front_end_publish"><input id="pid" type="hidden" name="pid" value="'.$post->ID.'" /><input id="FE_PUBLISH" type="hidden" name="FE_PUBLISH" value="FE_PUBLISH" /><input id="submit" type="submit" name="submit" value="Publish" />';
		if( !(get_post_status() == 'trash') ) { ?>
			- <a class="btn" onclick="return confirm('Are you sure you wish to delete post: <?php echo get_the_title() ?>?')"href="<?php echo get_delete_post_link( get_the_ID() ); ?>">Delete</a>
	<?php }
		echo '</form></p>';
	}
}

//function to update post status
function change_post_status($post_id,$status) {
	$current_post = get_post( $post_id, 'ARRAY_A' );
	$current_post['post_status'] = $status;
	wp_update_post($current_post);
}

if (isset($_POST['FE_PUBLISH']) && $_POST['FE_PUBLISH'] == 'FE_PUBLISH') {
	if (isset($_POST['pid']) && !empty($_POST['pid'])) {
		change_post_status((int)$_POST['pid'],'publish');
	}
};

/**
 * Show all parents, regardless of post status.
 *
 * @param   array  $args  Original get_pages() $args.
 *
 * @return  array  $args  Args set to also include posts with pending, draft, and private status.
 */
function my_slug_show_all_parents( $args ) {
	$args['post_status'] = array( 'publish', 'pending', 'draft', 'private' );
	return $args;
}
add_filter( 'page_attributes_dropdown_pages_args', 'my_slug_show_all_parents' );
add_filter( 'quick_edit_dropdown_pages_args', 'my_slug_show_all_parents' );
