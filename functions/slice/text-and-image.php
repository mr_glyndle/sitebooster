<?php function textandimage() {
background('text_image');
$alignment_over_background = '';
overlay($alignment_over_background);

$alignemnt = get_sub_field('alignment');
$distribution = get_sub_field('distribution');
$remove_image_space = get_sub_field('remove_image_space');

$vertical_image_alignment = get_sub_field_object('align_image');
$mobile_stack = get_sub_field('mob_stac');
$vertical_image_alignment = $vertical_image_alignment['value'];
if( $vertical_image_alignment == 'vtop' ) {
	$vertical_image_alignment = 'vtop';
} else if( $vertical_image_alignment == 'vbottom' ) {
	$vertical_image_alignment = 'vbottom';
} else  {
	$vertical_image_alignment = 'vmiddle';
};

$image_space_to_remove = '';
if( $remove_image_space ):
	foreach( $remove_image_space as $v ):
		$image_space_to_remove = $image_space_to_remove . 'nopad_' . $v . ' ';
	endforeach;
endif;
$image_and_text_image = get_sub_field('image_left_image');
$image_and_text_header = get_sub_field('image_left_header');
$image_and_text_text = get_sub_field('image_left_text');
$image_and_text_link_url = get_sub_field('link_button_url');
$image_and_text_link_text = get_sub_field('link_button_text');
$animate = ' ';
$animate = get_sub_field('animate');
if ($animate) {
	$animate = $animate[0];
};?>

	<div class="content t_and_i image_<?php echo $alignemnt . ' ' . $distribution; ?> item_<?php echo $GLOBALS['item_count']; if ($animate) { echo ' scrollme';}; if (!empty($mobile_stack)) { if( $mobile_stack[0] == 'img_t' ) { echo ' image_top'; } else if ($mobile_stack == 'img_b') { echo ' image_bottom'; }}; ?>">

		<?php if ($alignemnt == 'left') { ?>

		<div class="image inline-block <?php if ($image_space_to_remove) { echo $image_space_to_remove; }; echo $vertical_image_alignment;?>">

			<img class="<?php if ($animate) { echo 'animateme scrollme'; } else {echo 'lazyload'; };?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?>"
				<?php if ($animate) { include(locate_template('partials/animate.php')); }; ?>
				srcset="	<?php echo $image_and_text_image['sizes']['flex_height_medium']; ?> 898w, <?php echo $image_and_text_image['sizes']['flex_height_small']; ?> 500w"
				sizes="	(min-width: 992px) 45vw, 95vw"
				src="<?php echo $image_and_text_image['sizes']['flex_height_small']; ?>"
				alt=		"<?php echo $image_and_text_image['alt']; ?>"
				title=		"<?php echo $image_and_text_image['title']; ?>"
			/>

		</div>

		<?php }; ?>

	  <div 	class="text inline-block vmiddle">

		<?php echo $image_and_text_text; ?>

	  </div>

		<?php if ($alignemnt == 'right') { ?>

		<div class="image inline-block <?php if ($image_space_to_remove) { echo $image_space_to_remove; }; echo $vertical_image_alignment;?>">

			<img class="<?php if ($animate) { echo 'animateme'; };?> image_and_text_item_<?php echo $GLOBALS['item_count']; ?> lazyload"
				<?php if ($animate) { include(locate_template('partials/animate.php')); }; ?>
				srcset="<?php echo $image_and_text_image['sizes']['flex_height_medium']; ?> 898w, <?php echo $image_and_text_image['sizes']['flex_height_small']; ?> 500w"
				sizes="(min-width: 992px) 45vw, 95vw"
				src="<?php echo $image_and_text_image['sizes']['flex_height_small']; ?>"
				alt="<?php echo $image_and_text_image['alt']; ?>"
				title="<?php echo $image_and_text_image['title']; ?>"
			/>

		</div>

		<?php }; ?>

	</div>
</div>
</div>
<?php }
