<?php function standardslice() {

	background('text');
	$alignment_over_background = get_sub_field('bg_align');
	overlay($alignment_over_background);

	if (isset($alignment_over_background) && $alignment_over_background == 'normal') {
		echo '<div class="text_content">';
	};

	the_sub_field('txt_cont');

	if (isset($alignment_over_background) && $alignment_over_background == 'normal') {
		echo '</div>';
	};?>

	</div>
</div>
<?php }
