<?php function twocolumn() {

	background('two_column');
	$alignment_over_background = '';
	overlay($alignment_over_background);

	$intro_text = get_sub_field('intro_text');
	$distribution = get_sub_field('distribution');
	$align = get_sub_field('align');
?>
		<div class="two_column <?php echo $distribution; ?>">

			<?php if ($intro_text) {
				echo '<div class="intro">' . $intro_text . '</div>';
			}; ?>

			<div class="column left <?php echo 'v' . $align; ?>">
			    <?php the_sub_field('left_column'); ?>
			</div>
			<div class="column right <?php echo 'v' . $align; ?>">
			    <?php the_sub_field('right_column'); ?>
			</div>
		</div>
	</div>
</div>

<?php }
