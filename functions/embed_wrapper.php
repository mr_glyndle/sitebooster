<?php
add_filter('embed_oembed_html', 'my_embed_oembed_html', 99, 4);
function my_embed_oembed_html($html, $url, $attr, $post_id) {
  return '<div class="embed-container">' . $html . '</div>';
};

if ( ! isset( $content_width ) ) {
	$content_width = 3000;
}?>
