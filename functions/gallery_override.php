<?php
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby']) {
            unset($attr['orderby']);
		}
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

	$grid_item_count = 1;
	if(isset($item_count)) {
		++$item_count;
	} else {
		$item_count = 99;
	}

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {

		$img = wp_prepare_attachment_for_js($id);

		$image = wp_get_attachment_metadata( $id );

		$image_data = wp_get_attachment_image_src($id, 'flex_height_small');
		$image_data_large = wp_get_attachment_image_src($id, 'flex_height_xlarge');

		// Store the caption
	    $caption = $img['caption'];
		// Store the alt
	    $alt = $img['alt'];
		// Store the title
	    $title = $img['title'];

		$width = $image['sizes']['flex_height_small']['width'];
		$height = $image['sizes']['flex_height_small']['height'];

        $output .= '<div class="thumb ic_' . $grid_item_count . '">';
		$output .= '<a class="thumbnail_link lightmeup lightbox' . $item_count . '" href="' . $image_data_large[0] . '">';
		$output .= '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="' . $image_data[0] . '" alt="' . $alt . '" title="' . $title . '" class="lazyload" width="' . $width . '" height="' . $height . '"/>';
        $output .= '</a>';
		if ($caption) {
			$output .= '<div class="text"><p>' . $caption . '</p>';
			$output .= '</div></div>';
		} else {
			$output .= '</div>';
		};

		++$grid_item_count;
    }

	$before = '<div class="p-gall">';
	$GLOBALS['footer-js'] = $GLOBALS['footer-js'] . 'jQuery(".lightbox' . $item_count . '").featherlightGallery();';
    $after = '</div>';

	$output = $before . $output . $after;

	return $output;
};

// Add an svg icon to share buttons.
function my_share_social_icons( $html, $service ) {
    $icon_url = get_stylesheet_directory_uri() . '/img/symbol-defs.svg#icon-' . $service['id'];
    $icon_html = '<svg><use xlink:href="' . $icon_url . '"></use></svg>';
    return $html . $icon_html;
}
add_filter( 'dev_share_buttons_after_share_text', 'my_share_social_icons', 10, 2 );

add_filter( 'dev_share_buttons_share_api', '__return_true' );
?>
