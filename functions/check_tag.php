<?php
function check_tag($tag = '') {
	global $post;
	$taxonomy = "post_tag";

	if ( !in_the_loop() ) return false; // in-the-loop function

	$post_id = (int) $post->ID;

	$terms = get_object_term_cache($post_id, $taxonomy);
	if (empty($terms))
	       	$terms = wp_get_object_terms($post_id, $taxonomy);
	if (empty($terms)) return false;

	if (empty($tag)) return (!empty($terms));

	$tag = (array) $tag;
	foreach($terms as $term) {
		if ( in_array( $term->term_id, $tag ) )
			return true;
		elseif ( in_array( $term->name, $tag ) )
			return true;
		elseif ( in_array( $term->slug, $tag ) )
			return true;
	}

return false;
};?>
