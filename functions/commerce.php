<?php
remove_action( 'woocommerce_before_main_content','woocommerce_breadcrumb', 20, 0);
// Content wrapper
add_action('woocommerce_before_main_content', 'open_all', 5);

add_action( 'after_setup_theme', 'brandchap_shop_lightbox_setup' );
function brandchap_shop_lightbox_setup() {
	add_theme_support( 'wc-product-gallery-zoom' );
	add_theme_support( 'wc-product-gallery-lightbox' );
	add_theme_support( 'wc-product-gallery-slider' );
}

function open_all() {
	echo '<article>';?>
	<div class="container main <?php if ( is_active_sidebar( 'sidebar-1' ) ) { ?>sidebar<?php } else { ?>no_sidebar<?php }; ?>">
		<div class="products_container">
<?php }

// Closing div for our content wrapper
add_action('woocommerce_after_main_content', 'close_main', 50);
function close_main() {
    echo '</div><aside class="dark">';
}

// Closing div for our content wrapper
add_action('woocommerce_sidebar', 'close_all', 50);
	function close_all() {
    echo '</aside></div></article>';
}


/**
 * Optimize WooCommerce Scripts
 * Remove WooCommerce Generator tag, styles, and scripts from non WooCommerce pages.
 */
add_action( 'wp_enqueue_scripts', 'child_manage_woocommerce_styles', 99 );
function is_realy_woocommerce_page() {
        if(  function_exists ( "is_woocommerce" ) && is_woocommerce()){
                return true;
        }
        $woocommerce_keys   =   array ( "woocommerce_shop_page_id" ,
                                        "woocommerce_terms_page_id" ,
                                        "woocommerce_cart_page_id" ,
                                        "woocommerce_checkout_page_id" ,
                                        "woocommerce_pay_page_id" ,
                                        "woocommerce_thanks_page_id" ,
                                        "woocommerce_myaccount_page_id" ,
                                        "woocommerce_edit_address_page_id" ,
                                        "woocommerce_view_order_page_id" ,
                                        "woocommerce_change_password_page_id" ,
                                        "woocommerce_logout_page_id" ,
                                        "woocommerce_lost_password_page_id" ) ;
        foreach ( $woocommerce_keys as $wc_page_id ) {
                if ( get_the_ID () == get_option ( $wc_page_id , 0 ) ) {
                        return true ;
                }
        }
        return false;
}

function child_manage_woocommerce_styles() {
	 //remove generator meta tag
	 remove_action( 'wp_head', array( $GLOBALS['woocommerce'], 'generator' ) );

	 //dequeue scripts and styles
	 if ( !is_realy_woocommerce_page() ) {

		 // Dequeue scripts.
		 // wp_dequeue_script( 'wc_price_slider' );
		 // wp_dequeue_script( 'wc-single-product' );
		 // wp_dequeue_script( 'wc-add-to-cart' );
		 // wp_dequeue_script( 'wc-cart-fragments' );
		 // wp_dequeue_script( 'wc-checkout' );
		 // wp_dequeue_script( 'wc-add-to-cart-variation' );
		 // wp_dequeue_script( 'wc-single-product' );
		 // wp_dequeue_script( 'wc-cart' );
		 // wp_dequeue_script( 'wc-chosen' );
		 // wp_dequeue_script( 'woocommerce' );
		 // wp_dequeue_script( 'prettyPhoto' );
		 // wp_dequeue_script( 'prettyPhoto-init' );
		 // wp_dequeue_script( 'jquery-blockui' );
		 // wp_dequeue_script( 'jquery-placeholder' );
		 // wp_dequeue_script( 'fancybox' );
		 // wp_dequeue_script( 'jqueryui' );

		// Dequeue styles.
		// wp_dequeue_style('woocommerce-general');
		// wp_dequeue_style('woocommerce-layout');
		// wp_dequeue_style('woocommerce-smallscreen');
		// wp_dequeue_style( 'woocommerce_frontend_styles' );
		// wp_dequeue_style( 'woocommerce_fancybox_styles' );
		// wp_dequeue_style( 'woocommerce_chosen_styles' );
		// wp_dequeue_style( 'woocommerce_prettyPhoto_css' );

	} elseif ( is_realy_woocommerce_page() ) {

		function shop_footer_styles() {
			$filename = get_stylesheet_directory_uri() . '/css/shop/shop.css';
			$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
			$filenonroot = str_replace($root,"",$filename);
			if (file_exists($filenonroot)) {
			    $filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
			}
		    wp_enqueue_style( 'shop', $filename );
		};
		add_action( 'get_footer', 'shop_footer_styles' );

		// function shop_remove_styles() {
		// 	wp_dequeue_script( 'main-style' );
		// 	wp_dequeue_script( 'banner' );
		// 	wp_dequeue_style( 'category-styles' );
		// 	wp_dequeue_style( 'bulky-styles' );
		// };
		// add_action('wp_print_styles','shop_remove_styles',1);
	}
}
?>
