<?php
	function theme_enqueue_scripts() {
		$template_url = get_template_directory_uri();

		// jQuery.
		wp_enqueue_script( 'jquery' );

		// custom scripts

		$filename = get_template_directory_uri() . '/js/scripts-dist.js';
		$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		$filenonroot = str_replace($root,"",$filename);
		if (file_exists($filenonroot)) {
			$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
		}
		wp_enqueue_script( 'jscripts', $filename, array('jquery'),' ',true);

		if ( is_page_template( 'menu_page.php' ) || is_singular( 'menu' )  ) {

			function TBC_add_footer_styles() {

				$filename = get_stylesheet_directory_uri() . '/css/menu/menu.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
				    $filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
			    wp_enqueue_style( 'menu-styles', $filename );

			};
			add_action( 'get_footer', 'TBC_add_footer_styles' );

		} else if (is_archive()) {

			function TBC_add_footer_styles() {

				$filename = get_stylesheet_directory_uri() . '/css/category/category.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
				    $filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
			    wp_enqueue_style( 'category-styles', $filename );
			};
			add_action( 'get_footer', 'TBC_add_footer_styles' );

		} else {

			function TBC_add_footer_styles() {
				$filename = get_stylesheet_directory_uri() . '/style.css';
				$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
				$filenonroot = str_replace($root,"",$filename);
				if (file_exists($filenonroot)) {
				    $filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
				}
			    wp_enqueue_style( 'main-style', $filename );
			};
			add_action( 'get_footer', 'TBC_add_footer_styles' );

		}

		// Load Thread comments WordPress script
		if ( is_singular() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

	}
	add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts', 1 );

	// Registers an editor stylesheet for the theme
	function sitebooster_add_editor_styles() {

		$filename = get_stylesheet_directory_uri() . '/custom-editor-style.css';
		$root = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://' . $_SERVER['HTTP_HOST'] . '/';
		$filenonroot = str_replace($root,"",$filename);
		if (file_exists($filenonroot)) {
			$filename = $filename . '?m=' . date ("YmdHi", filemtime($filenonroot));
		}
	    add_editor_style( $filename );
	}
	add_action( 'init', 'sitebooster_add_editor_styles' );

	// include custom jQuery
	function include_custom_jquery() {
		if (!is_admin()) {
			$core_version = '3.3.1';
			wp_deregister_script('jquery');
			wp_enqueue_script('jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/' . $core_version . '/jquery.min.js', array(), null, false);
		}
	}
	add_action('wp_enqueue_scripts', 'include_custom_jquery');


// Remove Query Strings
function remove_cssjs_ver( $src ) {
if( strpos( $src, '?ver=' ) )
 $src = remove_query_arg( 'ver', $src );
return $src;
}
add_filter( 'style_loader_src', 'remove_cssjs_ver', 10, 2 );
add_filter( 'script_loader_src', 'remove_cssjs_ver', 10, 2 );

// Disable Emoticons
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Hide WordPress Version
remove_action( 'wp_head', 'wp_generator' ) ;
