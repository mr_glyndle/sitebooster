<?php
function background($slice_type) {

	// Slice type is set
	if (get_sub_field('sname')) {

		$slice_name = get_sub_field('sname');
		$GLOBALS['slice_name'] = get_sub_field('sname');
		$text_colour =  get_sub_field('txt_col');
		$bg_cont = get_sub_field('bg_cont');
		if ($slice_name) {
			$slice_name = preg_replace("/[^A-Za-z ]/", '', $slice_name);
			$slice_ID = str_replace(' ', '', $slice_name);
		};
		if(isset($bg_cont)) {
			// Use subfields to find settings

			// Colour or Gradient
			if ($bg_cont == "corg" && get_sub_field('bg_col')) {
				$bg_color = get_sub_field('bg_col');
				$bg_grad = get_sub_field('bg_grad');
				$grad_dir = get_sub_field('grad_dir');
			// Image
		} else if ($bg_cont == "image" && get_sub_field('bg_img')) {
				$bg_img = get_sub_field('bg_img');
			// Video
		} else if ($bg_cont == "video") {
				$bg_img = get_sub_field('vid_img');
				$bg_vid = true;
			// $bg_cont not set try to find fallback
			} else {
				if (get_sub_field('bg_col')) {
					$bg_color = get_sub_field('bg_col');
					$bg_grad = get_sub_field('bg_grad');
					$grad_dir = get_sub_field('grad_dir');
					$bg_cont = 'corg';
				} else if (get_sub_field('bg_img')) {
					$bg_img = get_sub_field('bg_img');
					$bg_cont = 'image';
				}
			}
		}
		// Is the background image position 'scroll' or 'fixed'?
		$bg_img_pos = get_sub_field('img_pos');

	// Menu slice
	} else if (isset($slice_type) && $slice_type == 'menu_layout') {

		$slice_name = get_field('sname');
		$GLOBALS['item_count'] = 0;
		$slice_type = 'top';
		$text_colour =  get_field('txt_col');
		$bg_cont = get_field('bg_cont');
		if ($slice_name) {
			$slice_name = preg_replace("/[^A-Za-z ]/", '', $slice_name);
			$slice_name = str_replace(' ', '_', $slice_name);
		};
		if(isset($bg_cont)) {
			// Colour or Gradient
			if ($bg_cont == "corg" && get_field('bg_col')) {
				$bg_color = get_field('bg_col');
				$bg_grad = get_field('bg_grad');
				$grad_dir = get_field('grad_dir');
			}
			// Image
			else if ($bg_cont == "image" && get_field('bg_img')) {
				$bg_img = get_field('bg_img');
			}
			// Video
			else if ($bg_cont == "video") {
				$bg_vid = true;
				$bg_img = get_sub_field('vid_img');
			// $bg_cont not set try to find fallback
			} else {
				if (get_field('bg_col')) {
					$bg_color = get_field('bg_col');
					$bg_grad = get_field('bg_grad');
					$grad_dir = get_field('grad_dir');
					$bg_cont = 'corg';
				} else if (get_field('bg_img')) {
					$bg_img = get_field('bg_img');
					$bg_cont = 'image';
				}
			}
		}
		// Is the background image position 'scroll' or 'fixed'?
		$bg_img_pos = get_field('img_pos');

	// No slice type set, must be header
	} else {

		$slice_name = 'header';
		$GLOBALS['item_count'] = 0;
		$slice_type = 'top';
		if(isset($head_set['txt_col'])) {
			$text_colour =  $head_set['txt_col'];
		}
		if(isset($head_set['bg_cont'])) {
			$bg_cont = $head_set['bg_cont'];
		}
		if(isset($bg_cont)) {
			if ($bg_cont == "corg") {
				$bg_color = $head_set['bg_col'];
				$bg_grad = $head_set['bg_grad'];
				$grad_dir = $head_set['grad_dir'];
			}
			if ($bg_cont == "image") {
				$bg_img = $head_set['bg_img'];
			}
			else if ($bg_cont == "video") {
				$bg_vid = true;
				$bg_img = $head_set['bg_img'];
			}
		} else {
			if (isset($head_set['bg_col'])) {
				$bg_color = $head_set['bg_col'];
				$bg_grad = $head_set['bg_grad'];
				$grad_dir = $head_set['grad_dir'];
				$bg_cont = 'corg';
			} else if (isset($head_set['bg_img'])) {
				$bg_img = $head_set['bg_img'];
				$bg_cont = 'image';
			}
		}
		if(isset($head_set['img_pos'])) {
			$bg_img_pos = $head_set['img_pos'];
		}
	}

	// Get BG img size URLs
	if (isset($bg_img)) {
		$bg_img = $bg_img['sizes'];
		$bgimg_xxl = $bg_img['flex_height_xxlarge'];
		$bgimg_xl = $bg_img['flex_height_xlarge'];
		$bgimg_l = $bg_img['flex_height_large'];
		$bgimg_m = $bg_img['flex_height_medium'];
	}

	// Get gradient direction
	if (isset($grad_dir)) {
		if ($grad_dir == 'top') {
			$w3_bg_grad_dir = 'bottom';
		} else if ($grad_dir == 'left') {
			$w3_bg_grad_dir = 'right';
		}
	}
	?><div <?php if (isset($slice_ID)) { echo 'id="' . $slice_ID . '"'; } else { echo 'id="slice_' . $GLOBALS['item_count'] . '"'; };?> class="slice <?php echo $slice_type . ' ' . $slice_name;?> slice_<?php echo $GLOBALS['item_count'];  echo ' ' . $text_colour;
		if ( (isset($bg_img_pos) && $bg_img_pos !== "scroll") || !isset($bg_img_pos) ) {
			if ( isset($bg_cont) && ($bg_cont == "image" && ($bg_img_pos !== "scroll" || !isset($bg_img_pos) ))) {
				echo ' parallax';
			} if ( $bg_cont == "video" ) {
				echo ' video parallax';
			}
		}
		if ( ( isset($bg_color) && isset($bg_grad) ) && ( $bg_img_pos !== "scroll" || !isset($bg_img_pos ) ) ) {
			echo ' parallax';
		};?> "><?php
		// if a colour or gradient is set...
		if ((isset($bg_cont)) && ($bg_cont == "corg")) {
			if (isset($bg_color) || isset($bg_grad)) {
				$GLOBALS['footer-css'] .= " .slice_" . $GLOBALS['item_count'] . " {";
				if ($bg_color) {
					$GLOBALS['footer-css'] .= "background:" . $bg_color . ";";
				};
				if ($bg_grad) {
					$GLOBALS['footer-css'] .= "background: linear-gradient(to " . $w3_bg_grad_dir . ", " . $bg_color . " 0%," . $bg_grad . " 100%);";
				};
				if ( $bg_img_pos !== "scroll") {
					$GLOBALS['footer-css'] .= "background-attachment: fixed;";
				}
				$GLOBALS['footer-css'] .= "}";
			}
		}
		// If a background image is set...
		if ((isset($bg_cont)) && ($bg_cont == "image" || $bg_cont == "video") && (isset($bg_img))) {

			// Is there a webp version of the image and does the browser support webp?
			$webpver = $bgimg_m . '.webp';
			$webpver = strstr($webpver, 'wp-content/');

			if( is_file($webpver) ) {

					$bgimg_xxlwebp = $bgimg_xxl . '.webp';
					$bgimg_xlwebp = $bgimg_xl . '.webp';
					$bgimg_lwebp = $bgimg_l . '.webp';
					$bgimg_mwebp = $bgimg_m . '.webp';

			} else {

					$bgimg_xxlwebp = $bgimg_xxl;
					$bgimg_xlwebp = $bgimg_xl;
					$bgimg_lwebp = $bgimg_l;
					$bgimg_mwebp = $bgimg_m;

			}

			$GLOBALS['footer-css'] .= ".slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_m ."');
			}
			.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
				background-image: url('" . $bgimg_mwebp ."');
			}
			@media screen and (max-width: 700px), screen and (min-height: 700px) {
				.slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_m ."');
				}
				.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_mwebp ."');
				}
			}
			@media screen and (min-width: 1000px), screen and (min-height: 700px) {
				.slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_l . "');
				}
				.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_lwebp . "');
				}
			}
			@media screen and (min-width: 1800px), screen and (min-height: 1000px) {
				.slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_xl . "');
				}
				.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_xlwebp . "');
				}
			}
			@media screen and (min-width: 2500px), screen and (min-height: 1800px) {
				.slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_xxl . "');
				}
				.webp-support .slice.slice_" . $GLOBALS['item_count'] . " {
					background-image: url('" . $bgimg_xxlwebp . "');
				}
			}";
		}
		if (isset($bg_vid)) {
			include(locate_template('partials/video_bg.php'));
		}

		// clear variables
		unset($bg_color);
		unset($bg_grad);
		unset($grad_dir);
		unset($w3_bg_grad_dir);
		unset($bg_img);
		unset($text_colour);
	}


function overlay(&$alignment_over_background) {
	$rgba_colour = '';
	$a = '';
	if (get_sub_field('sname')) {
		$rgba_colour = get_sub_field('over_color');
		if (get_sub_field('over_opac')) {
			$a = get_sub_field('over_opac');
		}
	} else if (isset($head_set['over_color'])) {
		$rgba_colour = $head_set['over_color'];
		if (isset($head_set['over_opac'])) {
			$a = $head_set['over_opac'];
		}
	} else if (get_field('sname')) {
		$rgba_colour = get_field('over_color');
		if (get_field('over_opac')) {
			$a = get_field('over_opac');
		}
	}
	if ($rgba_colour && $a) {
		$Hex_color = str_replace("#", "", $rgba_colour);
		if(strlen($Hex_color) == 3) {
			$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
			$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
			$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
		} else {
			$r = hexdec(substr($Hex_color,0,2));
			$g = hexdec(substr($Hex_color,2,2));
			$b = hexdec(substr($Hex_color,4,2));
		}
		$rgb = array($r, $g, $b);
		$Rgb_color = implode(", ", $rgb);
		$rgba_colour = 'rgba(' . $Rgb_color . ',' . $a . ')';
	};


	unset($add_vertical_space);
	$alignment_over_background = get_sub_field('bg_align');
	$remove_vertical_space = get_sub_field_object('v_space');
	$remove_vertical_space_value = $remove_vertical_space['value'];
	$remove_vertical_space_choices = $remove_vertical_space['choices'];
	$space_to_remove = '';
	if( $remove_vertical_space_value ):
		foreach( $remove_vertical_space_value as $v ):
			$space_to_remove = $space_to_remove . 'nopad_' . $v . ' ';
		endforeach;
	endif;
	$add_vertical_space = get_sub_field('add_space');
	$add_vertical_margin = get_sub_field('add_space_ext');
	$background_content = get_sub_field('bg_cont');
?>
<div <?php if (isset($rgba_colour) && $rgba_colour !== '') { echo 'style="background-color:' . $rgba_colour . '"'; } ?> class="content txt_blk s_over <?php if (isset($rgba_colour) && $rgba_colour !== '') { echo 'ocset'; };?> avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($alignment_over_background) { echo ' ' . $alignment_over_background; }; ?>"><?php }
