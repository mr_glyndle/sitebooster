<?php
add_post_type_support( 'page', 'excerpt' );
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

function get_excerpt($limit, $source = null){

    $excerpt = $source == "content" ? get_the_content() : get_the_excerpt();
    $excerpt = preg_replace(" (\[.*?\])",'',$excerpt);
    $excerpt = strip_shortcodes($excerpt);
    $excerpt = strip_tags($excerpt);

	if (strlen($excerpt) > $limit) {
	    $excerpt = substr($excerpt, 0, $limit);
	    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	    $excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
		$excerpt = $excerpt . '...';
	}
	return $excerpt;
}

// Show excerpts by default
add_filter('default_hidden_meta_boxes', 'show_hidden_meta_boxes', 10, 2);
function show_hidden_meta_boxes($hidden, $screen) {
    if ( 'post' == $screen->base ) {
        foreach($hidden as $key=>$value) {
            if ('postexcerpt' == $value) {
                unset($hidden[$key]);
                break;
            }
        }
    }
    return $hidden;
}

// // Make excerpts mandatory
// function mandatory_excerpt($data) {
// 	if ( $post = get_post( $post ) ) {
//
// 		    if (empty($data['post_excerpt']) && !isset($_GET['action'])) {
//
// 				if ($data['post_status'] === 'publish') {
// 					add_filter('redirect_post_location', 'excerpt_error_message_redirect', '99');
// 				}
//
// 				if ($data['post_status'] == 'publish' || $data['post_status'] == 'future' || $data['post_status'] == 'pending') {
// 					$data['post_status'] = 'draft';
// 				}
// 			}
// 			return $data;
//
// 		}
// 	}
// add_filter('wp_insert_post_data', 'mandatory_excerpt');
//
// // Excerpt error message redirect
// function excerpt_error_message_redirect($location) {
// 	remove_filter('redirect_post_location', __FILTER__, '99');
// 	return add_query_arg('excerpt_required', 1, $location);
// }
//
// // Mandatory excerpts warning notice
// function excerpt_admin_notice() {
// 	if (!isset($_GET['excerpt_required'])) return;
//
// 	switch (absint($_GET['excerpt_required'])) {
// 		case 1:
// 		$message = "<span style='font-weight: bold; color: #dc3232;'>'Excerpt' is required.</span> An excerpt will help your SEO. Try to make it an enticing short intro to the content. <i>If you don't see the excerpt field anywhere on this page, enable it in the 'screen options' tab above.</i>";
// 		break;
// 		default:
// 		$message = 'Unexpected error';
// 	}
//
// 	echo '<div id="notice" class="error"><p>' . $message . '</p></div>';
// }
// add_action('admin_notices', 'excerpt_admin_notice');
