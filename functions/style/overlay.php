<?php
function overlay(&$alignment_over_background) {
	$rgba_colour = '';
	$a = '';
	if (get_sub_field('sname')) {
		$rgba_colour = get_sub_field('over_color');
		if (get_sub_field('over_opac')) {
			$a = get_sub_field('over_opac');
		}
	} else if (isset($head_set['over_color'])) {
		$rgba_colour = $head_set['over_color'];
		if (isset($head_set['over_opac'])) {
			$a = $head_set['over_opac'];
		}
	} else if (get_field('sname')) {
		$rgba_colour = get_field('over_color');
		if (get_field('over_opac')) {
			$a = get_field('over_opac');
		}
	}
	if ($rgba_colour && $a) {
		$Hex_color = str_replace("#", "", $rgba_colour);
		if(strlen($Hex_color) == 3) {
			$r = hexdec(substr($Hex_color,0,1).substr($Hex_color,0,1));
			$g = hexdec(substr($Hex_color,1,1).substr($Hex_color,1,1));
			$b = hexdec(substr($Hex_color,2,1).substr($Hex_color,2,1));
		} else {
			$r = hexdec(substr($Hex_color,0,2));
			$g = hexdec(substr($Hex_color,2,2));
			$b = hexdec(substr($Hex_color,4,2));
		}
		$rgb = array($r, $g, $b);
		$Rgb_color = implode(", ", $rgb);
		$rgba_colour = 'rgba(' . $Rgb_color . ',' . $a . ')';
	};


	unset($add_vertical_space);
	$alignment_over_background = get_sub_field('bg_align');
	$remove_vertical_space = get_sub_field_object('v_space');
	$remove_vertical_space_value = $remove_vertical_space['value'];
	$remove_vertical_space_choices = $remove_vertical_space['choices'];
	$space_to_remove = '';
	if( $remove_vertical_space_value ):
		foreach( $remove_vertical_space_value as $v ):
			$space_to_remove = $space_to_remove . 'nopad_' . $v . ' ';
		endforeach;
	endif;
	$add_vertical_space = get_sub_field('add_space');
	$add_vertical_margin = get_sub_field('add_space_ext');
	$background_content = get_sub_field('bg_cont');
?>
<div <?php if (isset($rgba_colour) && $rgba_colour !== '') { echo 'style="background-color:' . $rgba_colour . '"'; } ?> class="content txt_blk s_over <?php if (isset($rgba_colour) && $rgba_colour !== '') { echo 'ocset'; };?> avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($alignment_over_background) { echo ' ' . $alignment_over_background; }; ?>"><?php }
