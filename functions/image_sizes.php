<?php
//Add thumbnail, automatic feed links and title tag support
add_theme_support( 'post-thumbnails' );

//Add SVG support
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

add_image_size( 'flex_height_small', 500, 9999, false );
add_image_size( 'flex_height_medium', 898, 9999, false );
add_image_size( 'flex_height_large', 1280, 9999, false );
add_image_size( 'flex_height_xlarge', 2000, 9999, false );
add_image_size( 'flex_height_xxlarge', 3000, 9999, false );

add_image_size( 'square', 500, 500, true );
add_image_size( 'square_medium', 898, 898, true );
add_image_size( 'square_large', 1280, 1280, true );

add_image_size( 'golden_small', 527, 371, true );
add_image_size( 'golden_medium', 898, 632, true );
add_image_size( 'golden_large', 1280, 791, true );

add_image_size( 'pano_small', 527, 201, true );
add_image_size( 'pano_medium', 898, 343, true );
add_image_size( 'pano_large', 1280, 489, true );
add_image_size( 'pano_x_large', 2000, 764, true );

if( function_exists('get_field')) {
	function my_login_logo() {
		$logo_png = get_field('logo_png', 'option');
		$logo_png_url = $logo_png['sizes']['flex_height_small'];
		$logo_png_width = $logo_png['sizes']['flex_height_small-width'];
		$logo_png_height = $logo_png['sizes']['flex_height_small-height'];
		if ($logo_png_url) {?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url('<?php echo $logo_png_url; ?>');
				height:<?php echo $logo_png_height;?>px;
				width: 320px;
				background-size: contain;
				background-repeat: no-repeat;
	        }
	    </style>
	<?php }
	}
	add_action( 'login_enqueue_scripts', 'my_login_logo' );
}
