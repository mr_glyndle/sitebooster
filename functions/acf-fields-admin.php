<?php
if( ( wp_get_theme() == 'Sitebooster' ) || current_user_can('administrator') ) {

	add_filter('acf/settings/save_json', 'my_acf_json_save_point');
	function my_acf_json_save_point( $path ) {
	    // update path
	    $path = get_template_directory() . '/../../../../../../master-custom-fields';
	    // return
	    return $path;
	}
	add_filter('acf/settings/load_json', 'my_acf_json_load_point');
	function my_acf_json_load_point( $paths ) {
	    // remove original path (optional)
	    unset($paths[0]);
	    // append path
	    $paths[] = get_template_directory() . '/../../../../../../master-custom-fields';
	    // return
	    return $paths;
	}
} else {
	add_filter('acf/settings/show_admin', '__return_false');
}




// Google maps API
$maps_api = get_field('maps_api', 'option');
if (!empty($maps_api)) {
	add_filter('acf/settings/google_api_key', function () {
		$maps_api = get_field('maps_api', 'option');
	    return $maps_api;
	});
}
