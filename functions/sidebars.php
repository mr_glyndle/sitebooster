<?php
add_action('widgets_init', 'theme_register_sidebar');
function theme_register_sidebar() {
	if ( function_exists('register_sidebar') ) {
		register_sidebar(array(
			'name'			=> 'Footer',
			'id'			=> 'footer',
			'before_widget'	=> '<div id="%1$s" class="widget inline-block vtop justify-item %2$s">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<h4>',
			'after_title'	=> '</h4>',
		));
		register_sidebar(array(
			'name'			=> 'Header',
			'id'			=> 'header',
			'before_widget'	=> '<div id="%1$s" class="widget inline-block vbaseline justify-item %2$s">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<h4>',
			'after_title'	=> '</h4>',
		 ));
 		register_sidebar(array(
 			'name'			=> 'Shop',
			'id'			=> 'sidebar-1',
			'before_widget'	=> '<div id="%1$s" class="widget inline-block vtop justify-item %2$s">',
			'after_widget'	=> '</div>',
			'before_title'	=> '<h4>',
			'after_title'	=> '</h4>',
 		));
		if( function_exists('get_field')) {
			if (get_field('header_text_colour', 'option')) {
				$nav_text_colour = get_field('header_text_colour', 'option');
			}
		}
		if(isset($nav_text_colour) && $nav_text_colour == 'dark') {
			register_sidebar(array(
				'name'			=> 'Pop Up',
				'id'				=> 'popup',
				'before_widget'	=> '<div class="pop_overlay dark %2$s"><div class="pop_content"><a href="#" class="btn overlay_close">X close</a>',
				'after_widget'	=> '</div><a href="#" class="bg overlay_close"></a></div>',
				'before_title'	=> '<h4>',
				'after_title'	=> '</h4>',
			));
		} else {
			register_sidebar(array(
				'name'			=> 'Pop Up',
				'id'				=> 'popup',
				'before_widget'	=> '<div class="pop_overlay light %2$s"><div class="pop_content"><a href="#" class="btn overlay_close">close</a>',
				'after_widget'	=> '</div><a href="#" class="bg overlay_close"></a></div>',
				'before_title'	=> '<h4>',
				'after_title'	=> '</h4>',
			));
		}
 	}
}; ?>
