<?php
if ( function_exists( 'register_nav_menus' ) ) {
  	register_nav_menus(
  		array(
  		  'main_menu' => 'Main Menu',
		  'sticky_menu' => 'Sticky Menu',
		  'footer_menu' => 'Footer Menu'
  		)
  	);
};
function add_theme_caps() {
	$role = get_role( 'editor' );
	$role->add_cap( 'edit_theme_options' );
	if (get_role( 'shop_manager' )) {
	    $role = get_role( 'shop_manager' );
	    $role->add_cap( 'edit_theme_options' );
	};
}
add_action( 'admin_init', 'add_theme_caps');

if( !current_user_can('administrator') ) {
	/** Diable Plugins and updates **/
	define('DISALLOW_FILE_MODS',true);
	add_action('admin_init', 'my_remove_menu_elements', 102);
	function my_remove_menu_elements()
	{
		remove_submenu_page( 'themes.php', 'theme-editor.php' );
	}
}

/**
 * Add search box to primary menu
 */
// function wpgood_nav_search($items, $args) {
//     // If this isn't the primary menu, do nothing
//     if( !($args->theme_location == 'main_menu') )
//     return $items;
//     // Otherwise, add search form
//     return $items . '<li class="search">' . get_search_form(false) . '</li>';
// }
// add_filter('wp_nav_menu_items', 'wpgood_nav_search', 10, 2);
