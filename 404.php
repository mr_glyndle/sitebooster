<?php get_header(); ?>

<div class="slice text standard">
	<div class="txt_blk normal s_over avs_10  avm_default clearfix">
		<div class="text_content">
			<div class="alert alert-info" style="text-align: center;">
				<h1>We're very sorry, we can't find the page you're looking for</h1>
				<p>Please use the navigation or, seach the site with the options in the navigation.</p>
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>
