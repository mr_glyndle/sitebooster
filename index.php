<?php get_header();

if ( !post_password_required() ) {

	if (have_posts()) {

		while (have_posts()) : the_post();

			// check if the flexible content field has rows of data
		    if( have_rows('cont') ) {

				$item_count = 1;
				$GLOBALS['item_count'] = "1";

				// loop through the rows of data
				while ( have_rows('cont') ) : the_row();

					include(locate_template('partials/slice_loop.php'));

				endwhile;

			} else {
				echo '<div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix"><div class="text_content">';
				echo the_content();
				echo '</div></div></div>';
		 	}

			if ( is_singular() && !is_page() ) {

				global $paged;
				global $wp_query;

				echo '<div class="txt_blk page_nav clearfix">';
				echo '<div class="alignleft">' . get_previous_post_link( '%link', '&laquo; Previous: %title', TRUE ) . '</div>';
				echo '<div class="alignright">' . get_next_post_link( '%link', 'Next: %title &raquo;', TRUE ) . '</div></div>';

				the_tags( '<div class="txt_blk page_nav tags clearfix">More:  ', '', '</div>' );

			};

			if (comments_open() || get_comments_number()) {
				echo '<div class="txt_blk comments">';
				comments_template();
				echo '</div>';
			};

		endwhile;

	} else { ?>

		<div class="txt_blk">
			<div class="alert alert-info">
				<h1>Sorry, we can't find the page you're looking for</h1>
				<p>Please use the navigation or, seach the site with the options in the menu.</p>
			</div>
		</div>

	<?php }

	}  else {
		echo '<div class="slice text standard"><div class="txt_blk normal s_over avs_default  avm_default clearfix"><div class="text_content">';
		echo get_the_password_form();
		echo '</div></div></div>';
	}

get_footer(); ?>
