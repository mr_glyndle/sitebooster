</div>
<?php if (!is_page_template('no_chrome.php')) {
$footer_background_colour = get_field('footer_background_colour', 'option');
$footer_text_colour = get_field('footer_text_colour', 'option');?>
<footer class="main <?php if ($footer_text_colour) {
	 echo $footer_text_colour; }
	 ; ?>">

	<div class="txt_blk">
		<div class="inline-block vtop">
			<?php wp_nav_menu(array('theme_location' => 'footer_menu', 'depth' => 2, 'container' => false)); ?>
			<?php dynamic_sidebar( 'footer' ); ?>
		</div>
			<?php include(locate_template('partials/contact_details.php')); ?>
			<?php /// OPENING HOURS
			    if (have_rows('opening_hours', 'option')) {
					echo '<div class="inline-block vtop opening_times right"><p><b>Opening Times</b></p><p>';
					$open_days = array('Mo' => '', 'Tu' => '', 'We' => '', 'Th' => '', 'Fr' => '', 'Sa' => '', 'Su' => '');
			        while (have_rows('opening_hours', 'option')) : the_row();

			            $closed = get_sub_field('closed');
			            $from   = $closed ? 'Closed' : get_sub_field('from');
			            $to     = $closed ? '' : get_sub_field('to');

						$days = get_sub_field('days');

						foreach ($days as $day) {

							if ($open_days[$day] == '') {
								$open_days[$day] .= '<span class="open__day">' . $day . '</span>: ';
							} else {
								$open_days[$day] .= ' / ';
							}
							if ($from == 'Closed') {
								$open_days[$day] .= ' <span class="open__from">' . $from . '</span>';
							} else {
							$open_days[$day] .= ' <span class="open__from">' . $from . '</span>-<span class="open__to">' . $to . '</span>';

							}
				        }
			        endwhile;

					foreach ($open_days as $day) { echo $day . '<br>'; }


					echo '</p>';
					$TBC_foot = get_field('tbc_footer', 'option');
					if (empty($TBC_foot) || $TBC_foot[0] !== "no") { ?>
						<p class="small sitebooster">Powered by <a target="_blank" rel="noopener" title="Local Business Branding, Marketing, and design from The Brand Chap" href="https://www.thebrandchap.com">SiteBooster</a></p>
					<?php };
					echo '</div>';
			    } ?>

	</div>
</footer>

</div>
<nav class="sticky">
	<?php wp_nav_menu(array('theme_location' => 'sticky_menu', 'depth' => 1, 'container' => false)); ?>
</nav>
<?php dynamic_sidebar( 'popup' );
} // close no chrome logic

wp_footer();

if ($GLOBALS['footer-js']) { ?><script> jQuery(document).ready(function() { <?php echo $GLOBALS['footer-js'];?> } ); </script><?php };

if (isset($GLOBALS['youtubeAPI'])) { ?><script><?php echo $GLOBALS['youtubeAPI'];?>}</script><?php }; ?>

<style>
<?php
if(get_field('header_backround_colour', 'option')) {
	$header_backround_colour = get_field('header_backround_colour', 'option');
	echo '.pop_overlay.active .pop_content { background: ' . $header_backround_colour . '}';
}
if ($GLOBALS['footer-css']) {
   echo $GLOBALS['footer-css'];
}
if($footer_background_colour) {
	echo 'footer.main { background-color: ' .  $footer_background_colour . '; }';
};?>
</style>
<?php
if ($GLOBALS['footer-schema'] !== '') {
	echo '<script type="application/ld+json">{';
	echo $GLOBALS['footer-schema'];
	echo '}</script>';
}
if( !current_user_can('editor') && !current_user_can('administrator') ) {
	$tracking_code_body_bottom = get_field('tracking_code_body_bottom', 'option');
	if ($tracking_code_body_bottom) {
		echo $tracking_code_body_bottom;
	}
} ?>
</body>
</html>
