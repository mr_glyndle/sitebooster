//<![CDATA[

// Make vanilla javascript - https://tobiasahlin.com/blog/move-from-jquery-to-vanilla-javascript/

jQuery(function() {

    jQuery(document).click(function(e) {

        var target = e.target;
        if (!jQuery(target).is('.menu-item-has-children a') && !jQuery(target).parents().is('.menu-item-has-children')) {
            jQuery('.sub-menu').removeClass('active');
            jQuery('.menu-item-has-children a').removeClass('active');
        } else if (jQuery(target).is('.menu-item-has-children > a') && !jQuery(target).is('.menu-item-has-children > a.active')) {
            jQuery('.menu-item-has-children a').removeClass('active');
            jQuery('.sub-menu').removeClass('active');
            jQuery(target).next('.sub-menu').addClass('active');
            jQuery(target).addClass('active');
			return false;
        } else if (jQuery(target).is('.menu-item-has-children > a.active')) {
            jQuery('.menu-item-has-children a').removeClass('active');
            jQuery('.sub-menu').removeClass('active');
			return false;
        };

        if (jQuery(target).is('.menu_toggle') || jQuery(target).parents().is('.menu_toggle')) {
            if (jQuery(target).is('.menu_toggle.active') || jQuery(target).parents().is('.menu_toggle.active')) {
                jQuery('.main-nav').removeClass('active');
                jQuery('.menu_toggle').removeClass('active');
            } else if (jQuery(target).is('.menu_toggle') || jQuery(target).parents().is('.menu_toggle')) {
                jQuery('.main-nav').addClass('active');
                jQuery('.menu_toggle').addClass('active');
            };

			return false;
        };
		if (jQuery(target).is('a.btn.accomfilter')) {
            jQuery('nav.accom.filters').toggleClass('open');
			return false;
        }
    });
	jQuery('li.overlay a').click(function() {
		jQuery( "li.overlay a" ).toggleClass( "active" );
		jQuery( ".pop_overlay" ).toggleClass( "active" );
		jQuery( "#wrapper" ).toggleClass( "popup" );
		return false;
	})
	jQuery('a.overlay_close').click(function() {
		jQuery( "li.overlay a" ).toggleClass( "active" );
		jQuery( ".pop_overlay" ).toggleClass( "active" );
		jQuery( "#wrapper" ).toggleClass( "popup" );
		return false;
	})
	jQuery('.pop_overlay.active .bg').click(function() {
		jQuery( "li.overlay a" ).toggleClass( "active" );
		jQuery( ".pop_overlay" ).toggleClass( "active" );
		jQuery( "#wrapper" ).toggleClass( "popup" );
		return false;
	})



    jQuery(document).ready(function() {

		document.body.className += " loaded";

        jQuery('.fluidbox__ghost').css({
            'transition-property': '',
            'transition-property': 'opacity, transform'
        });


        var jQuerypreviousScroll = 0;
		var jQuerymainHeight = jQuery( '#wrapper' ).height();
		var jQuerywindHeight = jQuery( window ).height();
		var jQueryfootHeight = jQuery( 'footer.main' ).height();
		var jQuerymainHeight = jQuerymainHeight - (jQuerywindHeight + jQueryfootHeight);
		var headerHeight = jQuery( '.hat' ).height();
		var navHeight = jQuery( '.get_around' ).height();

		jQuerywin = jQuery('#wrapper').width();
        jQuery( window ).scroll(function() {
            var jQuerycurrentScroll = jQuery(this).scrollTop();
            if (jQuerycurrentScroll > jQuerypreviousScroll) {
				if (jQuerycurrentScroll > 200) {
	                window.setTimeout(hideNav, 300);
				}
            } else {
                window.setTimeout(showNav, 300);
            }
			if (jQuerycurrentScroll > jQuerymainHeight) {
				jQuery('nav.sticky').addClass("boost");
			} else {
				jQuery('nav.sticky').removeClass("boost");
			}

			if (jQuerywin > 780) {
				if (jQuerycurrentScroll >= headerHeight) {
					jQuery('.hat').css("padding-bottom", navHeight);
					jQuery('.get_around').addClass("fix_top");
				} else {
					jQuery('.hat').css("padding-bottom", 0);
					jQuery('.get_around').removeClass("fix_top");
				}
			}

            jQuerypreviousScroll = jQuerycurrentScroll;
        });

		if (jQuerywin < 780) {
			var jQuerystickyHeight = jQuery('nav.sticky').height();
			jQuery('#wrapper').css("padding-bottom", jQuerystickyHeight);
		}
		jQuery( window ).resize(function() {
			var headerHeight = jQuery( 'header.top' ).height();
			var navHeight = jQuery( '.get_around' ).height();
			jQuerywin = jQuery('#wrapper').width();
			if (jQuerywin < 780) {
				var jQuerystickyHeight = jQuery('nav.sticky').height();
				jQuery('#wrapper').css("padding-bottom", jQuerystickyHeight);
			} else {
				jQuery('#wrapper').css("padding-bottom", 0);
			}
		});

		// Hide / show the master navigation menu
        function hideNav() {
            jQuery("[data-nav-status='toggle']").removeClass("is-visible").addClass("is-hidden");
        }

        function showNav() {
            jQuery("[data-nav-status='toggle']").removeClass("is-hidden").addClass("is-visible");
        };

        // Add Smooth scroll
        jQuery('a[href*="#"]:not([href="#"])').click(function() {
            var pattern = /#tab/;
            var thisHash = this.hash;
            //returns true or false...
            var exists = pattern.test(thisHash);
            if (exists) {
            // Is a tab, do nothing
            } else {
                if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                    var target = jQuery(this.hash);
                    target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
                    if (target.length) {
                        jQuery('html, body').animate({
                            scrollTop: target.offset().top
                        }, 600);
                        return false;
                    }
                }
            }
        });
    });

	// adds mobile class, and mobile os to html tag
	jQuery(document).ready(function($){
	  var deviceAgent = navigator.userAgent.toLowerCase();

	  if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
	    $('html').addClass('ios');
	    $('html').addClass('mobile');
	  }

	  if (deviceAgent.match(/android/)) {
	    $('html').addClass('android');
	    $('html').addClass('mobile');
	  }

	  if (deviceAgent.match(/blackberry/)) {
	    $('html').addClass('blackberry');
	    $('html').addClass('mobile');
	  }

	  if (deviceAgent.match(/(symbianos|^sonyericsson|^nokia|^samsung|^lg)/)) {
	    $('html').addClass('mobile');
	  }

	});
});
//]]>
//@prepros-prepend lib/owl.carousel.js
//@prepros-prepend lib/owl.autoplay.js
//@prepros-prepend lib/owl.navigation.js
//@prepros-prepend lib/owl.animate.js
//@prepros-prepend lib/debounce.js
//@prepros-prepend lib/featherlight.js
//@prepros-prepend lib/featherlight.gallery.js
//@prepros-prepend lib/jquery.scrollme.min.js
