<?php $link_to_svg = get_template_directory_uri () . '/img/symbol-defs.svg'; ?>
<form action="<?php echo home_url( '/' ); ?>" method="get" class="form-inline">
  <fieldset class="search">
		<div class="input-group">
			<input type="text" name="s" aria-label="Searchbox. Type your search here" placeholder="search" value="<?php the_search_query(); ?>" class="form-control" />
			<button type="submit" aria-label="Start search" name="start search" class="search submit"><svg class="icon icon-search"><use xlink:href="<?php echo $link_to_svg; ?>#icon-search"></use></svg></button>
		</div>
  </fieldset>
</form>
