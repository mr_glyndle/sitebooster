<?php
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'title-tag' );
add_theme_support( 'woocommerce' );

// Custom Image Sizes
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/image_sizes.php';

// Override Wordpress Gallery
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/gallery_override.php';

// Replace attachement links with 'Large' image size rather than 'full'
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/link_to_large_images.php';

// Add menu support and register main menu
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/nav_menus.php';

// Add Publish button
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/publish_private.php';

// Register sidebar(s)
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/sidebars.php';

// Excerpt
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/excerpt.php';

// Does the post have 'a_tag'?
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/check_tag.php';

// Embed wrapper
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/embed_wrapper.php';

// Add options page
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/options_page.php';

// Add Fluidbox class to image links
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/add_fluidbox_class.php';

// infinate scroll gallery category pages
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/infinate_scroll.php';

// TinyMCE additions
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/tinyMCE.php';

if( function_exists('get_field')) {

	// Maintenace mode - Show styled page - Needs ACF to work

	// To Put in maintenace mode add the following to  config.php
	// define('IN_MAINTENANCE', true);

	add_action( 'wp_loaded', function() {
	    global $pagenow;
	    if(
	        defined( 'IN_MAINTENANCE' )
	        && IN_MAINTENANCE
	        && $pagenow !== 'wp-login.php'
	        && ! is_user_logged_in()
	    ) {
	        header( 'HTTP/1.1 Service Unavailable', true, 503 );
	        header( 'Content-Type: text/html; charset=utf-8' );
	        if ( file_exists( WP_CONTENT_DIR . '/themes/sitebooster/maintenance.php' ) ) {
	            require_once( WP_CONTENT_DIR . '/themes/sitebooster/maintenance.php' );
	        }
	        die();
	    }
	});

	// Search Custom fields
	require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/search_custom_fields.php';

	// Custom Field Slice Titles
	require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/acf.php';

	// Load and Save Custom Fields
	require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/acf-fields-admin.php';
};

// Tribe Events hooks
if ( class_exists( 'Tribe__Events__Main' ) ) {
	require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/events.php';
}

// Woocommerce
if ( function_exists( 'is_woocommerce' ) ) {
    require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/commerce.php';
}

// Gravity Forms
if ( file_exists(WP_CONTENT_DIR . '/plugins/gravityforms/gravityforms.php') ) {
	require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/gravity_forms.php';
}

// Load site scripts.
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/site_scripts_and_styles.php';

// Theme Styles
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/styling.php';

// Theme Slices
require_once WP_CONTENT_DIR . '/themes/sitebooster/functions/slices.php';

// [page_title] shortcode to add page title using editor
function page_title_sc( ){
   return get_the_title();
}
add_shortcode( 'page_title', 'page_title_sc' );


setcookie(TEST_COOKIE, 'WP Cookie check', 0, COOKIEPATH, COOKIE_DOMAIN);
if ( SITECOOKIEPATH != COOKIEPATH ) setcookie(TEST_COOKIE, 'WP Cookie check', 0, SITECOOKIEPATH, COOKIE_DOMAIN);

@ini_set( 'upload_max_size' , '4M' );
@ini_set( 'post_max_size', '4M');
@ini_set( 'max_execution_time', '300' );

// remove wp version param from any enqueued scripts
function vc_remove_wp_ver_css_js( $src ) {
    if ( strpos( $src, 'ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
add_filter( 'script_loader_src', 'vc_remove_wp_ver_css_js', 9999 );
