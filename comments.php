<?php
 /*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?><div id="comments" class="comments-area"><?php if ( have_comments() ) : ?><h4 class="comments-title">Respond to <?php echo get_the_title();?></h4><!-- .comments-title --><ul class="media-list"><?php wp_list_comments(); ?></ul><!-- .comment-list --><?php endif; // have_comments() ?><?php
		// If comments are closed and there are comments, let's leave a little note, shall we?
		if ( ! comments_open() && get_comments_number() && post_type_supports( get_post_type(), 'comments' ) ) :
	?><p class="no-comments">Comments are closed.</p><?php endif; ?><?php paginate_comments_links(); ?><?php comment_form(); ?></div><!-- .comments-area -->
