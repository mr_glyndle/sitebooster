<?php
	get_header();

	$i = 1;
	$j = $wp_query->post_count;
	$queried_object = get_queried_object();
	$post_id = $queried_object->taxonomy.'_'.$queried_object->term_id;

	$category_layout = get_field('category_layout', $post_id);
	$text_align = get_field('text_align', $post_id);
	$txt_col = get_field('dark_or_light', $post_id);

	$site_width = get_field('site_width', 'option');
	$count = count($posts);
	$grid_item_count = 1;

	include(locate_template('partials/category/filters.php'));
?>

<section class="category <?php if (isset($txt_col)) { echo $txt_col; };?> <?php echo $category_layout . ' ' . $text_align; if ($site_width == 'full') { echo " full"; }; echo ' ti_' . $j; if ($j % 2 == 0) { echo ' even'; } else { echo ' odd'; };?> clearfix">

	<?php

	if (have_posts()) {
		if ($category_layout == 'list') { ?>

			<div id="main-container" class="contain main clearfix">

				<div class="row list">

					<?php include(locate_template('partials/category/list_view.php')); ?>

					<div class="<?php echo $category_layout; ?> pagination text-centered"><?php
						global $wp_query;

						$big = 999999999; // need an unlikely integer

						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
							'format' => '?paged=%#%',
							'current' => max( 1, get_query_var('paged') ),
							'total' => $wp_query->max_num_pages,
						) );
						?>
					</div>
				</div>
			</div>

		<?php } else if ($category_layout == 'card') { ?>

			<div class="grid_cont grid <?php if (isset($txt_col)) { echo $txt_col; };?>">
				<div class="s_over grid card <?php echo $text_align . ' '; if ($site_width == 'full') { echo " full"; }; ?>">
					<div class="contain avs_default">
						<div class="grid items ti_<?php echo $j; if ($site_width == 'full') { echo " full"; }; ?>">
							<?php include(locate_template('partials/category/card_view.php')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="<?php echo $category_layout; ?> pagination text-centered"><?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages,
				) );
				?>
			</div>

		<?php } else if ($category_layout == 'gallery') { ?>

			<div class="grid grid_cont full infinate_scroll <?php if (isset($txt_col)) { echo $txt_col; };?>">
				<div class="s_over overlay grid card <?php if (isset($text_align)) { echo $text_align; }; if ($site_width == 'full') { echo " full"; }; ?>">
					<div class="contain avs_default">
						<div class="items">
							<?php include(locate_template('partials/category/gallery_view.php')); ?>
						</div>
					</div>
				</div>
			</div>

			<noscript><div class="<?php echo $category_layout; ?> pagination text-centered"><?php
					global $wp_query;
					$big = 999999999; // need an unlikely integer
					echo paginate_links( array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $wp_query->max_num_pages,
					) );
					?></div></noscript>

		<?php } else { ?>

			<div class="grid_cont grid <?php if (isset($txt_col)) { echo $txt_col; };?>">
				<div class="s_over grid card <?php echo $text_align . ' '; if ($site_width == 'full') { echo " full"; }; ?>">
					<div class="contain avs_default">
						<div class="items">
							<?php include(locate_template('partials/category/card_view.php')); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="<?php echo $category_layout; ?> pagination text-centered"><?php
				global $wp_query;

				$big = 999999999; // need an unlikely integer

				echo paginate_links( array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $wp_query->max_num_pages,
				) );
				?>
			</div>

		<?php };
	} else { ?>

		<div id="main-container" class="contain container main clearfix">
			<div class="row list">
				<h2>Sorry, no content found. Please search a different category, widen your search, or remove some filters.</h2>
			</div>
		</div>

	<?php }; ?>

</section>

<?php get_footer(); ?>
